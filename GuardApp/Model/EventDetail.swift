/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct EventDetail : Mappable {
	var id : Int?
	var name : String?
	var message : String?
	var venue : String?
	var event_date : String?
	var card_theme_id : Int?
	var organizer_id : Int?
	var additional_attributes : String?
	var created_at : String?
	var updated_at : String?
	var latitude : Double?
	var longitude : Double?
	var message_date_time : String?
	var state : Int?
	var plan_id : Int?
	var is_canceled : Bool?
	var language : String?
	var instant_invitation_sent : Bool?
	var confirm : Bool?
	var is_paid : Bool?
	var from_time : String?
	var to_time : String?
	var plan : Plan?
	var attendees : [Attendees]?
	var messages : [Messages]?
	var guest_plans : [Guest_plans]?
	var payments : [Payments]?
	var organizer : Organizer?
	var total_number_of_guests : Int?
	var guests_attended : Int?
	var guest_yet_to_attend : Int?
	var invitation_message_deliver : Bool?
	var reminder_message_deliver : Bool?
	var cancelation_message_deliver : Bool?
	var thankyou_message_deliver : Bool?
	var can_add_attendees : Bool?
	var can_send_thankyou_msg : Bool?
	var selected_language : String?
	var confirm_event_details : Bool?
	var confirm_payment : Bool?
	var is_active : Bool?
    var is_completed : Bool?
    
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		message <- map["message"]
		venue <- map["venue"]
		event_date <- map["event_date"]
		card_theme_id <- map["card_theme_id"]
		organizer_id <- map["organizer_id"]
		additional_attributes <- map["additional_attributes"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		latitude <- map["latitude"]
		longitude <- map["longitude"]
		message_date_time <- map["message_date_time"]
		state <- map["state"]
		plan_id <- map["plan_id"]
		is_canceled <- map["is_canceled"]
		language <- map["language"]
		instant_invitation_sent <- map["instant_invitation_sent"]
		confirm <- map["confirm"]
		is_paid <- map["is_paid"]
		from_time <- map["from_time"]
		to_time <- map["to_time"]
		plan <- map["plan"]
		attendees <- map["attendees"]
		messages <- map["messages"]
		guest_plans <- map["guest_plans"]
		payments <- map["payments"]
		organizer <- map["organizer"]
		total_number_of_guests <- map["total_number_of_guests"]
		guests_attended <- map["guests_attended"]
		guest_yet_to_attend <- map["guest_yet_to_attend"]
		invitation_message_deliver <- map["invitation_message_deliver"]
		reminder_message_deliver <- map["reminder_message_deliver"]
		cancelation_message_deliver <- map["cancelation_message_deliver"]
		thankyou_message_deliver <- map["thankyou_message_deliver"]
		can_add_attendees <- map["can_add_attendees"]
		can_send_thankyou_msg <- map["can_send_thankyou_msg"]
		selected_language <- map["selected_language"]
		confirm_event_details <- map["confirm_event_details"]
		confirm_payment <- map["confirm_payment"]
		is_active <- map["is_active"]
        is_completed <- map["is_completed"]
	}

}
