/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct Organizer : Mappable {
	var id : Int?
	var email : String?
	var created_at : String?
	var updated_at : String?
	var first_name : String?
	var last_name : String?
	var phone_number : String?
	var city : String?
	var country : String?
	var gender : String?
	var is_verified : Bool?
	var terms_and_conditions : Bool?
	var auth_token : String?
	var country_code : String?
	var picture : Picture?
	var registration_id : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		email <- map["email"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		first_name <- map["first_name"]
		last_name <- map["last_name"]
		phone_number <- map["phone_number"]
		city <- map["city"]
		country <- map["country"]
		gender <- map["gender"]
		is_verified <- map["is_verified"]
		terms_and_conditions <- map["terms_and_conditions"]
		auth_token <- map["auth_token"]
		country_code <- map["country_code"]
		picture <- map["picture"]
		registration_id <- map["registration_id"]
	}

}