/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct Payments : Mappable {
	var id : Int?
	var amount : String?
	var eci : String?
	var customer_email : String?
	var card_holder_name : String?
	var expiry_date : String?
	var currency : String?
	var sdk_token : String?
	var card_number : String?
	var payment_option : String?
	var fort_id : String?
	var language : String?
	var response_code : String?
	var status : String?
	var authorization_code : String?
	var response_message : String?
	var customer_ip : String?
	var command : String?
	var merchant_reference : String?
	var event_id : Int?
	var organizer_id : Int?
	var created_at : String?
	var updated_at : String?
	var discount : String?
	var coupon_id : String?
	var actual_amount : String?
	var guest_plan_id : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		amount <- map["amount"]
		eci <- map["eci"]
		customer_email <- map["customer_email"]
		card_holder_name <- map["card_holder_name"]
		expiry_date <- map["expiry_date"]
		currency <- map["currency"]
		sdk_token <- map["sdk_token"]
		card_number <- map["card_number"]
		payment_option <- map["payment_option"]
		fort_id <- map["fort_id"]
		language <- map["language"]
		response_code <- map["response_code"]
		status <- map["status"]
		authorization_code <- map["authorization_code"]
		response_message <- map["response_message"]
		customer_ip <- map["customer_ip"]
		command <- map["command"]
		merchant_reference <- map["merchant_reference"]
		event_id <- map["event_id"]
		organizer_id <- map["organizer_id"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		discount <- map["discount"]
		coupon_id <- map["coupon_id"]
		actual_amount <- map["actual_amount"]
		guest_plan_id <- map["guest_plan_id"]
	}

}