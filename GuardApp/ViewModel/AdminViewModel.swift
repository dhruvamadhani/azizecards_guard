//
//  AdminViewModel.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 10/01/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

                                                        
class AdminViewModel: NSObject {

    let apiClient = BWHandler.sharedInstance()
     let currentVC = UIApplication.topViewController() as? AppViewController
     let helper = AppHelper.init()
    
    
    func adminLogin ( Email : String, password : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
            let dictData = ["email":Email,"password":password, "type":"admin"];
            // let params = ["event_card":dictData];
            WebApi.sharedInstance.adminLogin(param: dictData) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as? [String:Any] ?? [:]
                    
                    let resDict = dictData
                    
                    var adminUserObj : adminUser?
                    adminUserObj = Mapper<adminUser>().map(JSON: dictData)
                    
                    
                    callback(success,adminUserObj,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func adminGetEvents (param: [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.adminManageEvents(param: param) { (success, response, message, statusCode) in
                if(success){
                    let dicEvents = response as? [String:Any] ?? [:]
                    let arrEvents : [[String:Any]] = dicEvents["events"] as? [[String:Any]] ?? []
                    var eventDetailList : [EventDetail] = [];
                    eventDetailList = Mapper<EventDetail>().mapArray(JSONArray: arrEvents)
                    
                    callback(true,eventDetailList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    //Get tickets
    func adminGetTicketsEvents (param: [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.adminManageTickets(param: param) { (success, response, message, statusCode) in
                if(success){
                    let arrTickets = response as? [[String:Any]] ?? []
                   // let arrTickets : [[String:Any]] = dicEvents["events"] as? [[String:Any]] ?? []
                    var ticketDetailList : [TicketsDetail] = [];
                    ticketDetailList = Mapper<TicketsDetail>().mapArray(JSONArray: arrTickets)
                    
                    callback(true,ticketDetailList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    //Logout
      
      func logOutApi(callback:@escaping (Bool,Any?,String,Int) -> Void)  {
          
        WebApi.sharedInstance.logoutApi(type:"admin") { (success, response, message, statuscode) in
              
              print(success)
              print(response)
              print(message)
              print(statuscode)
              if(success == true){
                  let dictData = response as? [String:Any] ?? [:]
                                 
                                 
                                 callback(success,dictData,message,statuscode);
                             }else{
                                 callback(success,response,message,statuscode);
                             }
              
          }
      }
      
    //update ticket status
    
    func updateTicketStatus(ticket_id : String,params:[String:Any], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
          
          if(!AppHelper.isInternetConnected()){
              return callback(false,nil,InternetAlert.kInternetMessage,400)
          }else{
              let fcmToken = GlobalUtils.getInstance().getDeviceToken()
            
            
            WebApi.sharedInstance.updateTicketStatus(ticket_id: ticket_id, params: params) { (success, response, message, statusCode) in
                
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
                    let resDict = dictData
                    print(resDict)
                    callback(success,response,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
           
//            WebApi.sharedInstance.updateEventConfirmPayment(event_id:event_id,params:params) { (success, response, message, statusCode) in
//                  if(success){
//                      let dictData = response as! [String:Any]
//                      let resDict = dictData
//                      var eventDetailsList : EventDetails?
//                      eventDetailsList = Mapper<EventDetails>().map(JSON: resDict)
//                      callback(true,eventDetailsList,message,statusCode);
//                  }else{
//                      callback(success,response,message,statusCode);
//                  }
//
//              }
          }
      }
    
    
    func adminComments ( text : String,event_id:String ,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
         if(!AppHelper.isInternetConnected()){
             return callback(false,nil,InternetAlert.kInternetMessage,400)
         }else{
             let fcmToken = GlobalUtils.getInstance().getDeviceToken();
             let dictData = ["comment_text":text];
            
            WebApi.sharedInstance.sendAdminComment(param: dictData, event_id: event_id) { (success, response, message, statusCode) in
                
                if success{
                    let dictData = response as? [String:Any] ?? [:]
                    callback(success,response,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
            }

         }
     }
    //Show Ticket
    func adminShowTicket (ticket_id : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            
            WebApi.sharedInstance.getShowTicket(ticket_id: ticket_id) { (success, response, message, statusCode) in
                if success{
                    
                    let dictResponse : [String:Any] = response as? [String:Any] ?? [:]
                    var ticketDetail : TicketsDetail?
                    
                    ticketDetail = Mapper<TicketsDetail>().map(JSON: dictResponse)
                    callback(success,ticketDetail,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
            }
            
            
//            WebApi.sharedInstance.adminManageTickets(param: param) { (success, response, message, statusCode) in
//                if(success){
//                    let arrTickets = response as? [[String:Any]] ?? []
//                   // let arrTickets : [[String:Any]] = dicEvents["events"] as? [[String:Any]] ?? []
//                    var ticketDetailList : [TicketsDetail] = [];
//                    ticketDetailList = Mapper<TicketsDetail>().mapArray(JSONArray: arrTickets)
//
//                    callback(true,ticketDetailList,message,statusCode);
//                }else{
//                    callback(success,response,message,statusCode);
//                }
//
//            }
        }
    }
    
    
}
