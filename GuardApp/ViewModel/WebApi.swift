//
//  TestVC.swift
//  Nathdwara_Swift
//
//  Created by prompt on 13/08/16.
//  Copyright © 2016 prompt. All rights reserved.
//


import Foundation
import UIKit
import ObjectMapper
import Alamofire
class WebApi {
    
    static let sharedInstance: WebApi = {
        let instance = WebApi()
        return instance
    }()
    
    fileprivate func getDefaultErrorMsg() -> String{
        return NSLocalizedString("ERROR_MSG_1", comment: "")
    }
    
    fileprivate func getInternateErrorMsg() -> String{
        return NSLocalizedString("ERROR_MSG_INTERNET", comment: "")
    }
    
    fileprivate func callBackError(_ error:Error) -> Void{
        print(getDefaultErrorMsg())
    }
    
    
//    func doLogin(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = AuthApiMethodName.kSignIn;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func get_countries( callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = AuthApiMethodName.kGet_countries;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    //get_cities
//    func get_cities( param : [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = AuthApiMethodName.kGet_cities;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = param
//        
//        let ws = BWHandler.sharedInstance()
//        ws.makeQueryString(values: param)
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    //get CardTypes
//    func card_types( callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kCardTypes;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    
//    func getCards( cardID : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kCard + "\(cardID)/cards";
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = [:]
//        
//        let ws = BWHandler.sharedInstance()
//        
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    
//    func plansOfEvents(param: [String:Any],  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kPlanList ;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = param
//        
//        let ws = BWHandler.sharedInstance()
//        ws.makeQueryString(values: param)
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func plansOfPackageOptions(  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kPlanOfPackageOptions ;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = [:]
//        
//        let ws = BWHandler.sharedInstance()
//        
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func addInvitees(event_id : String, param:[String:Any], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/attendees";
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func addMsgAndColorTOEevntCards( param:[String:Any], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kAddMsgAndColorToEventCards;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func createEvent(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kCreateEvent;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func template_details( cardID : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kEventDetails + "\(cardID)";
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = [:]
//
//        let ws = BWHandler.sharedInstance()
//       // ws.makeQueryString(values: param)
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func doSignUp(param:Dictionary<String, Any>,profilePic:UIImage!, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = AuthApiMethodName.kSignUp;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let imageData = profilePic.pngData();
//        let ws = BWHandler.sharedInstance()
//         ws.uploadVideoFile(request, videoData: imageData, false) { (response) in
//             self.checkResponse(response, callback)
//         }
//    }
//    
//    
//    func sendMessage(param:Dictionary<String, Any>, event_id: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/messages";
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    //View Reports
//    func getViewReports( eventId : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kAddInvitees + "\(eventId)/view_reports";
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = [:]
//        
//        let ws = BWHandler.sharedInstance()
//        
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
    //Get Event details
    func getEventDetails( eventID : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddInvitees + "\(eventID)";
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
        
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
//    
//    //Get profile Details of user
//    func getProfileDetails(  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = UserApiName.kProfile;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = [:]
//        
//        let ws = BWHandler.sharedInstance()
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    
//    //get user inquiry subjects
//    func getUserInquirySubjects(  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = UserApiName.kUserInquirySubjects;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = [:]
//        
//        let ws = BWHandler.sharedInstance()
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    //Get open/Closed ticket status as user inquiry status
//    func getUserInquiryStatus( param : [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = UserApiName.kUserInquiryStatus ;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.headerParam = header
//        request.reqParam = param
//        
//        let ws = BWHandler.sharedInstance()
//        ws.makeQueryString(values: param)
//        ws.get(request, true) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    //Post raise ticket
//    func raiseTicket(param:Dictionary<String, Any>,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = UserApiName.kUserInquiryStatus;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func createGuard(param:Dictionary<String, Any>, event_id: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/security_guards";
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func update_password(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let url = WebServiceUrl.kBASEURL + "organizers/update_password"
//        
//        Alamofire.request(url, method: .put, parameters: param, encoding: JSONEncoding.default,headers:header )
//        .responseJSON { response in
//            print(response.result)
//            let bwresponse = BWResponse()
//            bwresponse.error = response.error;
//            bwresponse.resData = response.data
//            self.checkResponse(bwresponse, callback)
//        }
//    }
//    
//    func doSignOut(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = AuthApiMethodName.kSignOut;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
//    func forgotPassword(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = AuthApiMethodName.kForgetPassword;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.post(request) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }
//    
    func getTermsandConditions(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kGetTermsAndConditions;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.get(request, false) { (response) in
            self.checkResponse(response, callback)
        }
    }
    


    func getShowEvent(callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
          let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = GuardApiName.kSecurityShowEvent;
          let request = BWRequest(reqUrlComponent: methodName)
          request.headerParam = header
          request.reqParam = [:]
          
          let ws = BWHandler.sharedInstance()
          
          ws.get(request, true) { (response) in
              self.checkResponse(response, callback)
          }
      }
    
    func guardLogin(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = GuardApiName.kLoginGuard;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func adminLogin(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
           let header = GlobalUtils.getInstance().getHeaderParam();
            let methodName = AdminApiName.kLoginAdmin
           let request = BWRequest(reqUrlComponent: methodName)
           request.reqParam = param
           request.headerParam = header
           let ws = BWHandler.sharedInstance()
           ws.post(request) { (response) in
               self.checkResponse(response, callback)
           }
       }
    
    func securityEventsAttendee( event_id: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = GuardApiName.ksecurityEventsAttendee + "event_attendees";
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = [:]
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
         ws.get(request, false) { (response) in
                   self.checkResponse(response, callback)
        }
    }
    
    func adminManageEvents( param : [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
       let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AdminApiName.kEvents
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = param
        
        let ws = BWHandler.sharedInstance()
        ws.makeQueryString(values: param)
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func adminManageTickets( param : [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
       let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AdminApiName.kTicketDetails
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = param
        
        let ws = BWHandler.sharedInstance()
        ws.makeQueryString(values: param)
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    
    func updateTicketStatus( ticket_id: String,params:[String:Any],callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
              
              let header = GlobalUtils.getInstance().getHeaderParam();
              let url = WebServiceUrl.kBASEURL + "admins/tickets/\(ticket_id)"
              print("url:\(url)")
              print("body:\(params)")
              Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default,headers:header )
              .responseJSON { response in
                  print(response.result)
                  let bwresponse = BWResponse()
                  bwresponse.error = response.error;
                  bwresponse.resData = response.data
                  self.checkResponse(bwresponse, callback)
              }
    }
    
    
    func checkQRCode( ticket_id: String,params:[String:Any],callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
              
              let header = GlobalUtils.getInstance().getHeaderParam();
              let url = WebServiceUrl.kBASEURL + "security_guards/events/check_qrcode"
              print("url:\(url)")
              print("body:\(params)")
//        let arrParam : Parameters = [
//                                            params
//                                    ]
//                    print(arrParam)
              Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default,headers:header )
              .responseJSON { response in
                  print(response.result)
                  let bwresponse = BWResponse()
                  bwresponse.error = response.error;
                  bwresponse.resData = response.data
                  self.checkResponse(bwresponse, callback)
              }
    }
    
    func logoutApi( type : String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        let header = GlobalUtils.getInstance().getHeaderParam();
        let url = WebServiceUrl.kBASEURL + "logout"
        let params : Parameters = ["type":type]
        Alamofire.request(url, method: .delete, parameters: params, encoding: JSONEncoding.default,headers:header )
        .responseJSON { response in
            print(response.result)
            let bwresponse = BWResponse()
            bwresponse.error = response.error;
            bwresponse.resData = response.data
            self.checkResponse(bwresponse, callback)
        }
    }
    
    
    func sendAdminComment(param:Dictionary<String, Any>, event_id : String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
            let header = GlobalUtils.getInstance().getHeaderParam();
             let methodName = AdminApiName.kTicketDetails + "/\(event_id)/create_admin_comments"
            let request = BWRequest(reqUrlComponent: methodName)
            request.reqParam = param
            request.headerParam = header
            let ws = BWHandler.sharedInstance()
            ws.post(request) { (response) in
                self.checkResponse(response, callback)
            }
        }
    
    
    func getShowTicket(ticket_id : String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
          let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AdminApiName.kTicketDetails + "/\(ticket_id)"
          let request = BWRequest(reqUrlComponent: methodName)
          request.headerParam = header
          request.reqParam = [:]
          
          let ws = BWHandler.sharedInstance()
          
          ws.get(request, true) { (response) in
              self.checkResponse(response, callback)
          }
      }
    
    //---------------------------------------------------------------------
    // MARK: Check Response
    //---------------------------------------------------------------------
    func checkResponse(_ response:BWResponse?,_ callback:(Bool,Any?,String,Int) -> Void){
        
        // stage 1
        let error = response?.error
        if (error != nil ){
            
            callback(false,nil,"Something went wrong",400)
        }
        
        // stage 2
        let resData = response?.resData
        let response = response?.response as? HTTPURLResponse
        let statusCode = response?.statusCode ?? 400
        if resData != nil {
            do {
                let dic:Dictionary<String,Any> = try JSONSerialization.jsonObject(with: resData!, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String,Any>
                let isSuccess =  dic["success"] as? Bool ?? false;
                let message = dic["message"] as? String ?? "";
                if(isSuccess){
                    var data = dic["data"]
                    if(data == nil){
                        data = dic["date"]
                    }
                    callback(isSuccess,data,message,statusCode)
                }else{
                    
                    callback(isSuccess,nil,message,statusCode)
                }
                
            } catch {
                
                callback(false,nil,"No Data Found",statusCode)
            }
        }
    }
    
    
  
}

