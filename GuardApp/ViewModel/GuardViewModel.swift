//
//  GuardViewModel.swift
//  ECards
//
//  Created by Dhruva Madhani on 25/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

class GuardViewModel: NSObject {
    
    let apiClient = BWHandler.sharedInstance()
    let currentVC = UIApplication.topViewController() as? AppViewController
    let helper = AppHelper.init()
    
    
    func guardLogin ( phoneNumer : String, countryCode : String , password : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
            let dictData = ["phone_number":phoneNumer,"country_code":countryCode,"password":password,"type":"guard"];
            // let params = ["event_card":dictData];
            WebApi.sharedInstance.guardLogin(param: dictData) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as? [String:Any] ?? [:]
                    
                    let resDict = dictData
                    let authToken = dictData["auth_token"] as? String ?? ""
                    let userID = resDict["id"] as? Int ?? 0
                    if(authToken != ""){
                        GlobalUtils.getInstance().setSessionToken(token: authToken)
                        GlobalUtils.getInstance().setUserID(userID: userID);
                        GlobalUtils.getInstance().setLogin(islogin: true)
                        GlobalUtils.getInstance().setifUserLoggedIn(islogin: true)
                     //   let email : String = resDict["email"] as? String ?? ""
                        //GlobalUtils.getInstance().setEmail(email: email)
                    }
                    
                    
                    var guardObj : Guard?
                    guardObj = Mapper<Guard>().map(JSON: dictData)
                    
                    
                    callback(success,guardObj,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    func getEventDetails (cardId : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
       //     let params : [String:Any] = param
            
            WebApi.sharedInstance.getEventDetails(eventID: cardId) { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
                    let resDict = dictData
                    var eventDetail : EventDetails?
                    eventDetail = Mapper<EventDetails>().map(JSON: resDict)
                    callback(true,eventDetail,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
            }
        }
    }
    
    func getSecurityEventsAttendees (eventId : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
           
           if(!AppHelper.isInternetConnected()){
               return callback(false,nil,InternetAlert.kInternetMessage,400)
           }else{
          //     let params : [String:Any] = param
               
            WebApi.sharedInstance.securityEventsAttendee(event_id: eventId) { (success, response, message, statusCode) in
                   if(success){
//                       let dictData = response as? [String:Any] ?? [:]
//                       let resDict = dictData
//                       var eventDetail : EventDetails?
//                       eventDetail = Mapper<EventDetails>().map(JSON: resDict)
                       callback(true,response,message,statusCode);
                   }else{
                       callback(success,response,message,statusCode);
                   }
               }
           }
       }
    
      
        func getShowEvents( callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
               
               if(!AppHelper.isInternetConnected()){
                   return callback(false,nil,InternetAlert.kInternetMessage,400)
               }else{
              
                WebApi.sharedInstance.getShowEvent { (success, response, message, statusCode) in
                    
                    if success{
                         let dictData = response as? [String:Any] ?? [:]
                         let resDict = dictData
                         var eventDetail : EventDetail?
                         eventDetail = Mapper<EventDetail>().map(JSON: resDict)
                         callback(true,eventDetail,message,statusCode);
                    }else{
                       callback(true,response,message,statusCode);
                    }
                }
                
                   
//                WebApi.sharedInstance.securityEventsAttendee(event_id: eventId) { (success, response, message, statusCode) in
//                       if(success){
//    //                       let dictData = response as? [String:Any] ?? [:]
//    //                       let resDict = dictData
//    //                       var eventDetail : EventDetails?
//    //                       eventDetail = Mapper<EventDetails>().map(JSON: resDict)
//                           callback(true,response,message,statusCode);
//                       }else{
//                           callback(success,response,message,statusCode);
//                       }
//                   }
               }
           }
        
    
    
        
        func checkQRCode(ticket_id : String,params:[String:Any], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
              
              if(!AppHelper.isInternetConnected()){
                  return callback(false,nil,InternetAlert.kInternetMessage,400)
              }else{
                  let fcmToken = GlobalUtils.getInstance().getDeviceToken()
                
//
//                WebApi.sharedInstance.checkQRCode(ticket_id: ticket_id, params: params) { (<#Bool#>, <#Any?#>, <#String#>, <#Int#>) in
//                    <#code#>
//                }
                let param = ["tokens" : params]
                WebApi.sharedInstance.checkQRCode(ticket_id: ticket_id, params: params) { (success, response, message, statusCode) in

                    if(success){
                        let dictData = response as? [String:Any] ?? [:]
                        let resDict = dictData
                        print(resDict)
                        callback(success,response,message,statusCode);
                    }else{
                        callback(success,response,message,statusCode);
                    }

                }
               
  
              }
          }

    
    
    //logout

    func logOutApi(callback:@escaping (Bool,Any?,String,Int) -> Void)  {
        
      WebApi.sharedInstance.logoutApi(type:"guard") { (success, response, message, statuscode) in
            
            print(success)
            print(response)
            print(message)
            print(statuscode)
            if(success == true){
                let dictData = response as? [String:Any] ?? [:]
                               
                               
                               callback(success,dictData,message,statuscode);
                           }else{
                               callback(success,response,message,statuscode);
                           }
            
        }
    }
    
}
