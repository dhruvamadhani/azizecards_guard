//
//  CustomTextField.swift
//  BaseProject
//
//  Created by TpSingh on 11/04/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit
private var kAssociationKeyMaxLength: Int = 0
@IBDesignable class BWTextField: UITextField {

    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
      if action == #selector(UIResponderStandardEditActions.paste(_:)) ||  action == #selector(UIResponderStandardEditActions.cut(_:)){
        return false
      }
      return super.canPerformAction(action, withSender: sender)
    }
    var padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 40)
    var insets : UIEdgeInsets?
//    override open func textRect(forBounds bounds: CGRect) -> CGRect {
//        if !self.isSecureTextEntry{
//            padding = UIEdgeInsets(top:0,left:10,bottom:0,right:10)
//        }
//        return bounds.inset(by: padding)
//    }
//
//    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//
//        return bounds.inset(by: padding)
//    }
//
//    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
    var imageView = UIButton()
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 50{
        didSet {
                   updateView()
        }
    }
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
   func updateView() {
    
        let appLang = GlobalUtils.getInstance().getApplicationLanguage()
        if appLang == "arabic" {
            
            self.textAlignment = .right
        }else{
            self.textAlignment = .left
        }
    
       if let image = rightImage {
           
        
       // let language = LocalizationSystem.sharedInstance.getLanguage()
        
        imageView = UIButton(frame: CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25)))
        //if language == "en"{
            
//        }
//        if language == "ar" {
//            imageView.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
//        }
        
           
           imageView.contentMode = .scaleAspectFit
           imageView.setImage(image, for: .normal)
           imageView.setImage(image, for: .selected)
           self.clipsToBounds = true
        if(self.isSecureTextEntry == true){
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
        }
            if appLang == "arabic" {
                leftViewMode = .always
                leftView = imageView
                imageView.imageEdgeInsets = UIEdgeInsets(top: 0, left:0 , bottom: 0, right: -40)
                self.textAlignment = .right
            }else{
                rightViewMode = .always
                rightView = imageView
                imageView.imageEdgeInsets = UIEdgeInsets(top: 0, left: -40, bottom: 0, right: 0)
                self.textAlignment = .left
            }
        
           // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".

           
       }
     self.font = Font.tagViewFont;
    self.autocapitalizationType = .sentences
    attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color,NSAttributedString.Key.font :Font.tagViewFont])
   }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.isSecureTextEntry = !self.isSecureTextEntry;
        if(self.isSecureTextEntry == false){
            self.imageView.setImage(UIImage(named: "show_password"), for: .normal)
             self.imageView.setImage(UIImage(named: "show_password"), for: .selected)
        }else{
             self.imageView.setImage(UIImage(named: "hide_password"), for: .normal)
            self.imageView.setImage(UIImage(named: "hide_password"), for: .selected)
        }
        // Your action
    }

    
   
    
   
    
    @IBInspectable var maxLength: Int {
          get {
              if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                  return length
              } else {
                  return Int.max
              }
          }
          set {
              objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
              addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
          }
      }
      
      @objc func checkMaxLength(textField: UITextField) {
          guard let prospectiveText = self.text,
              prospectiveText.count > maxLength
              else {
                  return
          }
          
          let selection = selectedTextRange
          
          let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
          let substring = prospectiveText[..<indexEndOfText]
          text = String(substring)
          
          selectedTextRange = selection
      }
    
    private func setInsets(forBounds bounds: CGRect) -> CGRect {

        var totalInsets = insets //property in you subClass

        if let leftView = leftView  { totalInsets?.left += leftView.frame.origin.x }
        if let rightView = rightView { totalInsets?.right += rightView.bounds.size.width }
        if(totalInsets != nil){
            return bounds.inset(by: totalInsets!)
        }
        return bounds.inset(by: padding)
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return setInsets(forBounds: bounds)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return setInsets(forBounds: bounds)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return setInsets(forBounds: bounds)
    }

    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {

        var rect = super.rightViewRect(forBounds: bounds)
        rect.origin.x -= insets?.right ?? 0.0

        return rect
    }

    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {

        var rect = super.leftViewRect(forBounds: bounds)
        rect.origin.x += insets?.left ?? 0.0

        return rect
    }
    
}
