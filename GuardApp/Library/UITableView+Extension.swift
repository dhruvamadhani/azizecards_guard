//
//  UITableView+Extension.swift
//  GreetClub
//
//  Created by Padam on 10/07/19.
//  Copyright © 2019 Padam. All rights reserved.
//

import Foundation
import UIKit
extension UIScrollView
{
    
    func findRefreshControl () -> UIRefreshControl?
    {
        for view in subviews
        {
            if view is UIRefreshControl
            {
                print("The refresh control: \(view)")
                return view as? UIRefreshControl
            }
        }
        return nil
    }
    func addRefreshControlWith(sender: UIViewController, action: Selector, view: UIView)
    {
        guard findRefreshControl() == nil else { return }
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(sender, action: action, for: UIControl.Event.valueChanged)
       // refreshControl.attributedTitle = NSAttributedString(string: "Refreshing");
        refreshControl.tintColor = COLOR.navTintColor
        view.addSubview(refreshControl)
    }
    
    func showRefreshControlWith(attributedTitle: String? = nil)
    {
        findRefreshControl()?.attributedTitle = NSAttributedString(string: attributedTitle!)
        findRefreshControl()?.beginRefreshing()
    }
    
    func hideRefreshControl ()
    {
        findRefreshControl()?.endRefreshing()
    }
}
