 //
//  WSHandler.swift
//
//
//  Created by Bluewhale Apps on 26/06/19.
//  Copyright © 2019 Bluewhale Apps. All rights reserved.

import Alamofire
import Foundation
 
 extension Data {
    
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
 }
 
extension Dictionary {
    func merge(_ dict: Dictionary<Key,Value>) -> Dictionary<Key,Value> {
        var mutableCopy = self
        for (key, value) in dict {
            mutableCopy[key] = value
        }
        return mutableCopy
    }
    mutating func lowercaseKeys() {
           for key in self.keys {
               let str = (key as! String).capitalizingFirstLetter()
               self[str as! Key] = self.removeValue(forKey:key)
           }
    }
}
 
extension String {
     func capitalizingFirstLetter() -> String {
         return prefix(1).lowercased() + dropFirst()
     }

     mutating func capitalizeFirstLetter() {
         self = self.capitalizingFirstLetter()
     }
    func dateString() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .full
        dateFormatter.dateFormat = DateFormat.dateFormat;
        //dateFormatter.dateStyle = style
        return dateFormatter.date(from: self) ?? Date()
        
    }
 }
//---------------------------------------------------------------------
// MARK: PSRequest
//---------------------------------------------------------------------
class BWRequest: NSObject {
    
    var reqUrlComponent:String?
    var reqParam:Any?
    var reqParamArr:Array<Dictionary<String, Any>> = []
    var headerParam:Dictionary<String,String> = [:]
    var data: Data?
    var method:String?
    init(reqUrlComponent:String) {
        self.reqUrlComponent = reqUrlComponent
    }
}

class BWResponse: NSObject {
    var response:URLResponse?
    var resData:Data?
    var error:Error?
    var jsonType:Int?
}

//---------------------------------------------------------------------
// MARK: WebService Handler
//---------------------------------------------------------------------
class BWHandler: NSObject,URLSessionDelegate {
    fileprivate static var obj:BWHandler?
    let kBASEURL  = WebServiceUrl.kBASEURL
    let kTimeOutValue = 60
   
   let sessionToken:String? = nil
    var apiUrl:URL?
    
    static func create() -> BWHandler{
        if(obj == nil){
            obj = BWHandler()
        }
        return obj!
    }
    
    static func sharedInstance() -> BWHandler{
        if(obj == nil){
            return create()
        }
        else{
            return obj!
        }
    }
    
    //---------------------------------------------------------------------
    // MARK: appDefaultHedaer
    //---------------------------------------------------------------------
    func appDefaultHedaer() -> Dictionary<String,String>{
        let dic:Dictionary<String,String> = ["Content-Type":"application/json"]
        return dic
    }
    
    //---------------------------------------------------------------------
    // MARK: POST Web Service methods
    //---------------------------------------------------------------------
    
    func post(_ bwRequest:BWRequest,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        post(bwRequest,true, completionHandler: completionHandler )
    }
    
    func post(_ bwRequest:BWRequest,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        let urlComponent = bwRequest.reqUrlComponent
        if urlComponent == nil {return}
        
        var urlSTR = ""
        
       urlSTR = kBASEURL + urlComponent!
        urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
        self.apiUrl = URL(string: urlSTR)
        var request = URLRequest(url: self.apiUrl!)
        request.httpMethod =  bwRequest.method ?? "POST"
        request.timeoutInterval =  TimeInterval(kTimeOutValue);
        request.allHTTPHeaderFields = bwRequest.headerParam
        if(bwRequest.method == "PUT"){
            request.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        }
        // Set request param
        let reqParam = bwRequest.reqParam
        if(reqParam != nil){
            do{
                let postData = try JSONSerialization.data(withJSONObject: reqParam, options:.prettyPrinted)
                let decoded = try JSONSerialization.jsonObject(with: postData, options: [])
                // here "decoded" is of type `Any`, decoded from JSON data
                let jsonString = String(data: postData, encoding: .utf8)
                print(jsonString!)
                // you can now cast it with the right type
                if decoded is [String:AnyObject] {
                    // use dictFromJSON
                }
                request.httpBody = postData
            }catch {
                let res = BWResponse()
                res.error = nil
                completionHandler(nil)
            }
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
       
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            if data != nil{
                let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("@API: RESPONCE:\(datastring!)")
                print("\n@API: -----------------------------------------------")
            }
            
            let res = BWResponse()
            res.response = response
            
            if let resError = error {
                res.error = resError
            }
            
            if let resData = data {
                res.resData = resData
            }
             completionHandler(res)
        })
        print("@API: -----------------------------------------------\n")
        print("@API: URL:\(urlSTR)\n")
        print("@API: PARAM:\(reqParam)\n")
        
        dataTask.resume()
    }
    
    
    //---------------------------------------------------------------------
    // MARK: GET Web Service methods
    //---------------------------------------------------------------------
    
    //==============================================================
    public func makeQueryString(values: Dictionary<String,Any>) -> String {
        var querySTR = ""
        if values.count > 0 {
            querySTR = "?"
            for item in values {
                let key = item.key
                let value = item.value as! String
                let keyValue = key + "=" + value + "&"
                querySTR = querySTR.appending(keyValue)
            }
            querySTR.removeLast()
        }
        return querySTR
    }
    
    func get(_ psRequest:BWRequest,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        
        let urlComponent = psRequest.reqUrlComponent
        
        if urlComponent == nil {return}
        
        let dictData = psRequest.reqParam
        let param = dictData as? Dictionary<String,Any> ?? [:]
        let querySTR = makeQueryString(values: param)
    
        var urlSTR = kBASEURL + urlComponent! + querySTR
        
        urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
        let url = URL(string: urlSTR)
        var request = URLRequest(url: url!)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = psRequest.headerParam
        request.timeoutInterval =  180;

        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            if data != nil{
                let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("@API: RESPONCE:\(datastring!)")
                print("\n@API: -----------------------------------------------\n")
            }
            
            let res = BWResponse()
            res.response = response
            
            if let resError = error {
                res.error = resError
            }
            
            if let resData = data {
                res.resData = resData
            }
            
            //TODO: Send response back in BG thread and shift to main therad in controller only
            //DispatchQueue.main.async {
                completionHandler(res)
            //}
        })
        
        print("@API: -----------------------------------------------\n")
        print("@API: URL:\(urlSTR)\n")
        //print("@API: PARAM:\(params!)\n")
        dataTask.resume()
    }
    
    
    
 

    private func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    
    func uploadVideoFile(_ bwRequest:BWRequest,videoData:Data?,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
    //        if(videoData == nil){
    //             completionHandler(nil)
    //            return;
    //        }
            let urlComponent = bwRequest.reqUrlComponent
            
            if urlComponent == nil {return}
            
            var urlSTR = ""
            
            urlSTR = kBASEURL + urlComponent!
            urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
            self.apiUrl = URL(string: urlSTR)
            let headerParam = ["Content-Type":"multipart/form-data"]
            let reqParam = bwRequest.reqParam as! [String:Any]
            let params = reqParam as? [String:Any] ?? [:]
            var body = Data()
            
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in reqParam {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if let data = videoData{
                    multipartFormData.append(data, withName: "user[picture]", fileName: "image.png", mimeType: "image/png")
                }
                
            }, usingThreshold: UInt64.init(), to:self.apiUrl!, method: .post, headers: headerParam) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        let res = BWResponse()
                        
                        
                        if let resError = response.error {
                            res.error = resError
                        }
                        if let resData = response.data {
                            res.resData = resData
                        }
                       completionHandler(res)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    let res = BWResponse()

                        res.error = error
                    completionHandler(res)
                }
            }
    }
 }
