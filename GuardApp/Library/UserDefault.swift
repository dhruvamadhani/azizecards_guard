//
//  UserDefault.swift
//  BaseProject
//
//  Created by Aj Mehra on 08/03/17.
//  Copyright © 2017 Debut Infotech. All rights reserved.
//

import Foundation
import UIKit


extension UITextView {

    private class PlaceholderLabel: UILabel { }

    private var placeholderLabel: PlaceholderLabel {
        if let label = subviews.compactMap( { $0 as? PlaceholderLabel }).first {
            return label
        } else {
            let label = PlaceholderLabel(frame: .zero)
            label.font = font
            addSubview(label)
            return label
        }
    }

    @IBInspectable
    var placeholder: String {
        get {
            return subviews.compactMap( { $0 as? PlaceholderLabel }).first?.text ?? ""
        }
        set {
            let placeholderLabel = self.placeholderLabel
            placeholderLabel.text = newValue
            placeholderLabel.font = Font.tagViewFont
            placeholderLabel.textColor = UIColor.lightGray
            placeholderLabel.numberOfLines = 0
            let width = frame.width - textContainer.lineFragmentPadding * 2
            let size = placeholderLabel.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude))
            placeholderLabel.frame.size.height = size.height
            placeholderLabel.frame.size.width = width
            if(GlobalUtils.getInstance().getApplicationLanguage() == "arabic"){
                placeholderLabel.frame.origin = CGPoint(x: textContainer.lineFragmentPadding - 20, y: textContainerInset.top)

            }else{
                placeholderLabel.frame.origin = CGPoint(x: textContainer.lineFragmentPadding + 8, y: textContainerInset.top)

            }
            
            textStorage.delegate = self
        }
    }

}

extension UITextView: NSTextStorageDelegate {

    public func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
        if editedMask.contains(.editedCharacters) {
            placeholderLabel.isHidden = !text.isEmpty
        }
    }

}

enum DefaultKey {
  case user
  case token
  case enableLog
  case firstLaunch
  case introScreen
}

class Defaults {
  //MARK:- SingleTon
  static let shared = Defaults()
  
  //MARK:- Variables
  let userDefault = UserDefaults.standard
    
  //MARK:- Setter
  func set(value:Any,  forKey key:DefaultKey) {
    userDefault.set(value, forKey: String(describing: key))
    userDefault.synchronize()
  }
  
  //MARK:- Getter
  func get(forKey key:DefaultKey) -> Any? {
    return userDefault.object(forKey: String(describing: key))
  }
  
  //MARK:- Methods
  func removeAll() {
//    User.sharedInstance.resetUserInstance()
    let appDomain = Bundle.main.bundleIdentifier
    userDefault.removePersistentDomain(forName: appDomain!)
    userDefault.synchronize()
  }
  
  func remove(_ key:DefaultKey) {
    userDefault.removeObject(forKey: String(describing: key))
    userDefault.synchronize()
  }
}
