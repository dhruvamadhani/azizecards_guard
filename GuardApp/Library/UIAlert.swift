//
//  UIAlert.swift
//  UNMASK
//
//  Created by Reactive Space on 2/27/18.
//  Copyright © 2018 Owais Akram. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog
// for showing alert

extension UIViewController{
    
    func showAlert(message: String, title: String)
    {
//        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
//        let OkAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alertController.addAction(OkAction)
//        self.present(alertController, animated: true, completion: nil)
        self.popupAndsetMessageOninfoClick(title: title, Message:message)
    }
    func popupAndsetMessageOninfoClick(title : String, Message : String)  {
          
          let popUp = PopupDialog(title: title, message: Message)
          
          let btnOk = CancelButton(title: AppHelper.localizedtext(key: "OK")) {
              print("You okied the car dialog.")
          }
          popUp.addButtons([btnOk])
          
          // Present dialog
          self.present(popUp, animated: true, completion: nil)
      }
    
    //-----------------------------------------------------------
    func DefalutDialoguesettingOfPopup(){
        let dialogAppearance = PopupDialogDefaultView.appearance()
        
        dialogAppearance.backgroundColor      = UIColor(red:0.23, green:0.23, blue:0.27, alpha:1.00)
        dialogAppearance.titleFont            = .boldSystemFont(ofSize: 14)
        dialogAppearance.titleColor           = UIColor(named: "SecondrayColor")
        dialogAppearance.titleTextAlignment   = .center
        dialogAppearance.messageFont          = .systemFont(ofSize: 14)
        dialogAppearance.messageColor         = UIColor(named: "DarkGray")
        let appLang = GlobalUtils.getInstance().getApplicationLanguage()
        if(appLang == "arabic"){
         dialogAppearance.titleTextAlignment   = .center
         dialogAppearance.messageTextAlignment = .right
        
        }else{
         dialogAppearance.titleTextAlignment   = .center
         dialogAppearance.messageTextAlignment = .left
         
        }
        
        
        let pcv = PopupDialogContainerView.appearance()
        pcv.backgroundColor = UIColor(red:0.23, green:0.23, blue:0.27, alpha:1.00)
     //   pcv.cornerRadius    = 2
        pcv.shadowEnabled   = true
        pcv.shadowColor     = .black
        
        
        // Customize dialog appearance
        let pv = PopupDialogDefaultView.appearance()
        pv.titleFont    = UIFont(name: "Zeit-Light", size: 16)!
      //  pv.titleColor   = .white
        pv.messageFont  = UIFont(name: "Zeit-Light", size: 14)!
      //  pv.messageColor = UIColor(white: 0.8, alpha: 1)
    }
    
    func showErrorMsg(message:String)
    {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = .gray
//        loadingIndicator.startAnimating();
//
//        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        // set the timer
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.hideIndicator), userInfo: nil, repeats: false)
    }
    
    
    
    func showIndicator(message:String)
    {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                loadingIndicator.hidesWhenStopped = true
                loadingIndicator.style = .gray
                loadingIndicator.startAnimating();
        
               alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        // set the timer
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.hideIndicator), userInfo: nil, repeats: false)
    }
    
    @objc func hideIndicator()
    {
        dismiss(animated: false, completion: nil)
        
    }
}
