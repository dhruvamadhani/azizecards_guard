//
//  AppViewController.swift
//  ShortTrip
//
//  Created by prompt on 26/05/17.
//  Copyright © 2017 Prompt Softech. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SVProgressHUD
import Alamofire
import PopupDialog
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UILabel {
    func halfTextColorChange (fullText : String , changeText : String ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named:"SecondrayColor") , range: range)
        attribute.addAttribute(NSAttributedString.Key.underlineStyle, value: UIColor(named:"SecondrayColor") , range: range)
        attribute.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: range)
        self.attributedText = attribute
    }
}

class AppViewController: UIViewController,UIGestureRecognizerDelegate {
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
 //   var payFortModel = PayFortModel()
    var order_id = 356
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let picker = UIPickerView()
    var refreshControl = UIRefreshControl()
    let toolBar = UIToolbar();
 
    var societyParam:Dictionary<String,Any> = [:]
    
    // MARK: View Life Cycle ------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DefalutDialoguesettingOfPopup()
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
//
       // self.view.backgroundColor = COLOR.backGroundColor
        
            
        let appearance = UITableView.appearance()
       
      //  self.navigationController?.navigationBar.prefersLargeTitles = true
        appearance.tableFooterView = UIView(frame: CGRect.zero);
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: COLOR.titleColor,
             NSAttributedString.Key.font: Font.titleFont]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: Font.barButtonFont], for: .normal)
        UITableViewCell.appearance().backgroundColor = UIColor.clear;
        
        self.navigationController?.navigationBar.isTranslucent = false;
        
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
         self.navigationController?.navigationBar.tintColor = COLOR.textColor;
//        refreshControl.tintColor = UIColor.blue
//        let logo = UIImage(named: "app_logo")
//        let imageView = UIImageView(image:logo)
//        imageView.contentMode = .scaleAspectFill
//        imageView.clipsToBounds = true
//        self.navigationItem.titleView = imageView
        let lang = GlobalUtils.getInstance().getApplicationLanguage()
    //    self.navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func setTitleView(){
        let logo = UIImage(named: "app_top")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //self.navigationController?.navigationBar.tintColor = COLOR.navBarColor
    }
    
    
    //---------------------------------------------------------------------
    // MARK: Round Image
    //---------------------------------------------------------------------
    func RoundImage(img: UIImageView)  {
        img.layer.cornerRadius = img.frame.size.width / 2
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
    }
    func isValidEmail(textStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: textStr)
    }
    
    func isValidPasswordFormat(textStr: String) -> Bool {
        let pwRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,15}"
        let pwTest = NSPredicate(format:"SELF MATCHES %@", pwRegEx)
        return pwTest.evaluate(with: textStr)
    }
    
    func startAnimating(){
        self.view.isUserInteractionEnabled = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
       
    }
    func stopAnimating(){
        self.view.isUserInteractionEnabled = true
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
       
    }
    
    //---------------------------------------------------------------------
    // MARK: Handle Invalid Token
    //---------------------------------------------------------------------
    

    //---------------------------------------------------------------------
    // MARK: Alert View
    //---------------------------------------------------------------------
    
    func showAlert(title:String, message:String) {
        self.popupAndsetMessageOninfoClick(title: title, Message:message)
    }
    
    func showAlertWithCallBack(title:String, message:String,callBack:@escaping () -> Void) {
        
        let popUp = PopupDialog(title: title, message: message)
            let btnOk = CancelButton(title: AppHelper.localizedtext(key: "OK")) {
                print("You okied the car dialog.")
                callBack()
            }
            popUp.addButtons([btnOk])
            
            // Present dialog
            self.present(popUp, animated: true, completion: nil)
        
      //  self.popupAndsetMessageOninfoClick(title: title, Message:message)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func  progressColor(value:Int) -> UIColor {
        if value < 70 {
            return UIColor.red
        }
        else if value >= 70 && value < 85 {
            return UIColor.yellow
        }
        else if value >= 85 && value < 100 {
            return UIColor.blue
        }
        else{
            return UIColor.green
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func showActivityIndicatory(isLoading:Bool = true) {
        DispatchQueue.main.async {
                    SVProgressHUD.show(withStatus: "Loading...")
        }
       
//        if(isLoading == true){
//            container.frame = CGRect(x: 0, y:0, width: self.view.bounds.width, height:self.view.bounds.height)
//            loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
//            loadingView.center = container.center
//            loadingView.backgroundColor = COLOR.navTintColor
//            loadingView.clipsToBounds = true
//            loadingView.layer.cornerRadius = 10
//            activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
//            activityIndicator.style = .whiteLarge
//            self.view.isUserInteractionEnabled = false
//            activityIndicator.color = COLOR.navTintColor
//            activityIndicator.center = CGPoint(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 2);
//            loadingView.addSubview(activityIndicator)
//            container.addSubview(loadingView)
//            self.view.addSubview(activityIndicator)
//            activityIndicator.startAnimating()
//        }
        
    }
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    func exportVideo(videoURL:URL)  {
        let asset = AVAsset(url: videoURL) // video url from library
        let fileMgr = FileManager.default
        let dirPaths = fileMgr.urls(for: .documentDirectory, in: .userDomainMask)
        
        let filePath = dirPaths[0].appendingPathComponent("Video_\(datePresentString()).mp4") //create new data in file manage .mp4
        
        let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)
        
        // remove file if already exits
        let fileManager = FileManager.default
        do {
            try? fileManager.removeItem(at: filePath)
            
        } catch {
            print("can't")
        }
        
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.metadata = asset.metadata
        exportSession?.outputURL = filePath
        
        exportSession?.exportAsynchronously {
            if exportSession?.status == .completed {
                
            } else if exportSession?.status == .cancelled {
                print("AV export cancelled.")
            } else {
                print("Error is \(String(describing: exportSession?.error))")
            }
        }
    }
    public func datePresentString() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
        
        let iso8601String = dateFormatter.string(from: date as Date)
        
        return iso8601String
    }
    
    //------------------------------
      func getDayInfo(_ todayDate: Date) -> String? {
          let formatter  = DateFormatter()
          formatter.dateFormat = "yyyy-MM-dd"
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "EEEE"
          return dateFormatter.string(from: todayDate).capitalized
      }
    
      //------------------------------
      func getMonthInfo(_ todayDate: Date) -> String? {
          let formatter  = DateFormatter()
          formatter.dateFormat = "MM-yyyy"
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "MM"
          return dateFormatter.string(from: todayDate).capitalized
      }
    
      //------------------------------
      func getYearInfo(_ todayDate: Date) -> String? {
          let formatter  = DateFormatter()
          formatter.dateFormat = "MM-yyyy"
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy"
          return dateFormatter.string(from: todayDate).capitalized
      }
}

