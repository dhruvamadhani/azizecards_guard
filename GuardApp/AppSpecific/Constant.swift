//
//  Constant.swift
//  ShortTrip
//
//  Created by Abhishek Yadav on 13/05/17.
//  Copyright © 2017 Prompt Softech. All rights reserved.
//
import Foundation
import UIKit
//let bundleIdentifier = "com.GreetClub"
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate;

let kAppName = Bundle.main.localizedInfoDictionary?["CFBundleName"] as? String ?? "Aziz ECards"

struct Instagram {
//    static let kClientId = "e771fd21ba434cbaa2e4e436af928811"
//    static let kRedirectUri = "http://com.greetapp"
    static let kClientId = "e771fd21ba434cbaa2e4e436af928811"
    static let kRedirectUri = "http://com.greetapp"
}
struct storyBoard{
        static let kMainStoryboard = UIStoryboard(name: "Main", bundle: nil);
        static let kCustomerStoryBoard = UIStoryboard(name: "Customer", bundle: nil);
        static let kInfluencerStoryBoard = UIStoryboard(name: "Influencer", bundle: nil);
}


struct StaticPages {
    static let kTermsURL = "https://www.google.com"
}



enum StatusCode: Int {
    typealias RawValue = Int
    case parameterMissing =  1003
    case success =  200
    case requestFailed =  1001
    case invalidJson =  1002
    case dataNotFound =  100
    case paramsNotAvailable =  300
    case userTypeNotValid =  400
    case alreadyRegistered =  500
    case invalidUserNameOrPassword = 405
    case userNotFound = 404
    case passwordTokenExpired = 406
    case bankAccountNotAdded = 606
    case userUnAuthorized = 408
    case somethingWentWrong = 407
    case accountBlockedOrRemovd = 600
    case unknownException = 700
}

enum RequestStatus: String {
    typealias RawValue = String
    case pending =  "Pending"
    case approved =  "Approved"
    case rejected =  "Rejected"
    case completed =  "Completed"
}


enum NotificationType: String {
    typealias RawValue = String
    case requestApproved =  "RequestApproved"
    case requestReceived =  "RequestReceived"
    case requestRejected =  "RequestRejected"
    case profileApproved =  "ProfileApproved"
    case profileRejected =  "ProfileRejected"
    case requestCompleted =  "RequestCompleted"
}

enum RateType: String {
    typealias RawValue = String
    case regular =  "Regular"
    case expendited =  "Expendited"
    
}

struct  WebServiceUrl{
    static let pageCount = 10;
   // static let kBASEURL = "http://e81767a0.ngrok.io/api/"
    static let kBASEURL = "http://3.6.10.190/api/v1/"
}
struct WebServiceCall {
    
    static let KContentType = "Content-Type"
    static let KContentValue = "application/json"
    static let kAuthorizationToken = "AuthorizationToken"
}

struct Font {
    static let regularFont = UIFont(name: "Zeit-Light", size: 20)!;
    static let titleFont  = UIFont(name: "Zeit-Light", size: 20)!;
    static let barButtonFont  = UIFont(name: "Zeit-Light", size: 18)!;
    static let tagViewFont  = UIFont(name: "Zeit-Light", size: 14)!;
    static let tabbarFont  = UIFont(name: "Zeit-Light", size: 12)!;
}


struct COLOR{
    static let navBarColor = UIColor(named: "SecondrayColor")!
   // static let navBarColor = UIColor(patternImage: UIImage(named: "background_image")!);
    static let navTintColor = UIColor(named: "backgroundColor")!
    static let titleColor = UIColor.black
    static let backGroundColor = UIColor.clear
    static let tabbarSelectedColor = UIColor(named: "SecondrayColor")!
    static let tabbarColor = UIColor(named: "backgroundColor")!
    static let textColor = UIColor(named: "SecondrayColor")!
}

struct Settings{
    static let kEditProfile = AppHelper.localizedtext(key: "editProfile")
    static let kBankDetails = AppHelper.localizedtext(key: "bankDetails")
    static let kPriceDetails = AppHelper.localizedtext(key: "priceDetails")
    static let kMoneyEarned = AppHelper.localizedtext(key: "moneyEarned")
    static let kEditLanguage = AppHelper.localizedtext(key: "editLanguage")
    static let kEditCharity = AppHelper.localizedtext(key: "editCharity")
    static let kHowtoUse = AppHelper.localizedtext(key: "howtoUse")
    static let kFAQ = AppHelper.localizedtext(key: "faq")
    static let kAllRequests = AppHelper.localizedtext(key: "allRequests")
    static let kContactUs = AppHelper.localizedtext(key: "contactus")
    static let kSignOut = AppHelper.localizedtext(key: "signout")
    static let kSignIn = AppHelper.localizedtext(key: "signin")
    static let kPaymentInfo = AppHelper.localizedtext(key: "paymentInfo")
    static let kAboutApp = AppHelper.localizedtext(key: "aboutApp")
    static let kChangeLang = AppHelper.localizedtext(key: "changeAppLang")
    
}
struct DateFormat{
    static let dateFormat = "MM/dd/yyyy"
    static let timeFormat = "MM/dd/yyyy"
}


struct AlertLogout {
    static let klogOutTitle = AppHelper.localizedtext(key: "logoutMsg")
}


struct AlertHeader {
    static let kTermsConditions = AppHelper.localizedtext(key: "termsButtonTitle") as? String ?? ""
    static let kSignInTitle = AppHelper.localizedtext(key: "signInButtonTitle") as? String ?? ""
    static let kSignUPTitle = AppHelper.localizedtext(key: "signupButtonTitle") as? String ?? ""
    
}

struct InternetAlert {
    static let kInternetMessage = AppHelper.localizedtext(key: "kInternetMessage")
}

struct ValidationAlert {
    static let kNoData =  AppHelper.localizedtext(key: "kNoData")
    static let kEmptyPwd =  AppHelper.localizedtext(key: "kEmptyPwd")
    static let kEmptyConfirmPwd = AppHelper.localizedtext(key: "kEmptyConfirmPwd")
    static let kMatchPwd =  AppHelper.localizedtext(key: "kMatchPwd")
    static let kEmptyEmail =  AppHelper.localizedtext(key: "kEmptyEmail")
    static let kValidEmail =  AppHelper.localizedtext(key: "kValidEmail")
    static let kTermsConditions =  AppHelper.localizedtext(key: "kTermsConditions")
    static let kBankAccountNumber =  AppHelper.localizedtext(key: "kBankAccountNumber")
    static let kRoutingNumber =  AppHelper.localizedtext(key: "kRoutingNumber")
    static let kRoutingValidation =  AppHelper.localizedtext(key: "kRoutingValidation")
    static let kBankAccountValidation =  AppHelper.localizedtext(key: "kBankAccountValidation")
    static let kEmptyFirstName =  AppHelper.localizedtext(key: "kEmptyFirstName")
    static let kEmptyLastName =  AppHelper.localizedtext(key: "kEmptyLastName")
    static let kEmptyAliasName =  AppHelper.localizedtext(key: "kEmptyAliasName")
    static let kEmptyGender =  AppHelper.localizedtext(key: "kEmptyGender")
    static let kEmptyProfession =  AppHelper.localizedtext(key: "kEmptyProfession")
    static let kEmptyBio =  AppHelper.localizedtext(key: "kEmptyBio")
    static let kVideoAlert =  AppHelper.localizedtext(key: "kVideoAlert")
    static let kGOVIDAlert =  AppHelper.localizedtext(key: "kGovIDAlert")
    static let kEmptyEmailMSG = AppHelper.localizedtext(key: "kEmpltyEmailValidation")
    static let kEmailInvalidMSG = AppHelper.localizedtext(key: "kEmailValidation")
    static let kEmptyPasswordMSG = AppHelper.localizedtext(key: "kEmpltyPasswordValidation")
    static let kEmptyConfirmPasswordMSG = AppHelper.localizedtext(key: "kEmpltyConfirmPasswordValidation")
    static let kPasswordMismatchMSG = AppHelper.localizedtext(key: "kPasswordMismatch")
    static let kUserTypeMSG = AppHelper.localizedtext(key: "kUserType")
    
    static let kEmptyRegularPrice =  AppHelper.localizedtext(key: "kEmptyRegularPrice")
    static let kEmptyLanguageSelect =  AppHelper.localizedtext(key: "kEmptyLanguageSelect")
    static let kEmptySubjectSelect =  AppHelper.localizedtext(key: "kEmptySubjectSelect")
    static let kEmptyExpressPrice =  AppHelper.localizedtext(key: "kEmptyExpressPrice")
    static let kEmptyRegularDuration =  AppHelper.localizedtext(key: "kEmptyRegularDuration")
    static let kEmptyExpressDuration =  AppHelper.localizedtext(key: "kEmptyExpressDuration")
    
    static let kEmptyMessage =  AppHelper.localizedtext(key: "kEmptyMessage")
    static let kEmptyDescription =  AppHelper.localizedtext(key: "kEmptyDescription")
    static let kEmptyRateType =  AppHelper.localizedtext(key: "kEmptyRateType")
    static let kEmptyLanguage =  AppHelper.localizedtext(key: "kEmptyLanguage")
    static let kEmptyPronounciation =  AppHelper.localizedtext(key: "kEmptyPronounciation")
    static let kEmptyBirthDate =  AppHelper.localizedtext(key: "kEmptyBirthDate")
    static let kEmptyLocation =  AppHelper.localizedtext(key: "kEmptyLocation")
    static let kEmptyProfilePicture =  AppHelper.localizedtext(key: "kEmptyProfilePicture")
    static let kEmptySessionPrefMinutes =  AppHelper.localizedtext(key: "kEmptySessionPrefMinutes")
    static let kEmptySessionPrefCount =  AppHelper.localizedtext(key: "kEmptySessionPrefCount")
    
    static let kEmptyStartTime =  AppHelper.localizedtext(key: "kEmptyStartTime")
    static let kEmptyEndTime =  AppHelper.localizedtext(key: "kEmptyEndTime")
    static let kInvalidDigitsOfPhoneNumber = AppHelper.localizedtext(key: "kInvalidDigitsOfPhoneNumber")
    
    static let kEmptyCity = AppHelper.localizedtext(key: "kEmptyCity")
    static let kEmptyImage =  AppHelper.localizedtext(key: "kEmptyImage")
    static let kNumberSentText = AppHelper.localizedtext(key: "PhoneNumberSentText")
    static let kMaxLimitOfCard = AppHelper.localizedtext(key: "kMaxLimitOfCardText")
    static let kDeleteMessage = AppHelper.localizedtext(key: "DeleteMessage")
    static let kConfirmDeleteMessage = AppHelper.localizedtext(key: "ConfirmDeleteMessage")
    static let kEmptyCheckboxTick = AppHelper.localizedtext(key: "kEmptyCheckboxTick")
   static let kEmptyInstantMessage = AppHelper.localizedtext(key: "kEmptyInstantMessage")
    static let kEmptyConatctToImport = AppHelper.localizedtext(key: "kEmptyConatctToImport")
    static let kErrorInvitationDate = AppHelper.localizedtext(key: "kErrorInvitationDate")
    static let kSameTimeError = AppHelper.localizedtext(key: "kSameTimeError")
    static let kErrorChartGenerate = AppHelper.localizedtext(key: "kErrorChartGenerate")
    static let kEmptyToWhom = AppHelper.localizedtext(key: "kEmptyToWhom")
    static let kConfirmationCancelMessage = AppHelper.localizedtext(key: "kConfirmationCancelMessage")
    static let kDeleteEventMessage = AppHelper.localizedtext(key: "kDeleteEventMessage")
    static let kEventCancel = AppHelper.localizedtext(key: "kEventCancel")
    static let kEmptyPhoneCode = AppHelper.localizedtext(key: "kEmptyPhoneCode")
    static let kEmptyName = AppHelper.localizedtext(key: "kEmptyName")
    static let kDeleteConatct = AppHelper.localizedtext(key: "kDeleteConatct")
    static let kEmptySubjectType = AppHelper.localizedtext(key: "kEmptySubjectType")
    static let kEmptyCommet = AppHelper.localizedtext(key: "kEmptyComment")
    
    static let kEmptyEventName = AppHelper.localizedtext(key: "kEmptyEventName")
    static let kEmptyEventDate = AppHelper.localizedtext(key: "kEmptyEventDate")
    static let kEmptyEventLocation = AppHelper.localizedtext(key: "kEmptyEventLocation")
    static let kEmptyNoOfAttendess = AppHelper.localizedtext(key: "kEmptyNoOfAttendess")
    static let kChooseLanguage = AppHelper.localizedtext(key: "kChooseLanguage")
    static let kEmptyMessageDate = AppHelper.localizedtext(key: "kEmptyMessageDate")
    static let kEndTime = AppHelper.localizedtext(key: "kEndTime")
    static let kAcceptTermsCondition = AppHelper.localizedtext(key: "kAcceptTermsCondition")
    static let kEmptyPhoneNumber = AppHelper.localizedtext(key: "kEmptyPhoneNumber")
    static let kInvalidPhone = AppHelper.localizedtext(key: "kInvalidPhone")
    static let kEmptyCountry = AppHelper.localizedtext(key: "kEmptyCountry")
    static let kMinimumPassword = AppHelper.localizedtext(key: "kMinimumPassword")
    static let kThankYouMsgDeliverError = AppHelper.localizedtext(key: "kThankYouMsgDeliverError")
    static let kReminderMsgDeliverError = AppHelper.localizedtext(key: "kReminderMsgDeliverError")
    static let kEmptyContacts = AppHelper.localizedtext(key: "kEmptyContacts")
    static let kLanguageChangeAlert = AppHelper.localizedtext(key: "kLanguageChangeAlert")
}


struct ErrorMessage{
    static let kEmptyEmailMSG = AppHelper.localizedtext(key: "kEmpltyEmailValidation")
    static let kNoData = "No data found!!"
    static let kEmptyPwd = "Please enter password."
    static let kEmptyNewPwd = "Please enter new password."
    static let kEmptyConfirmPwd = "Please enter re-type password."
    static let kMatchPwd = "Password doesn't match."
    static let kEmptyFirstName = "Please enter first name."
    static let kEmptyLastName = "Please enter last name."
    static let kEmptyFMiddleName = "Please enter middle name."
    static let kEmptyCountry = "Please choose country name."
    static let kEmptyCity = "Please choose city."
    static let kEmptyGender = "Please select gender."
    static let kEmptyPhoneCode = "Please enter phone code."
    static let kEmptyPhoneNumber = "Please enter phone number."
    static let kEmptyEventName = "Please enter name of an event."
    static let kEmptyEventDate = "Please enter date of event."
    static let kEmptyEventLocation = "Please enter event location."
    static let kEmptyEventPackageOptions = "Please enter package Options."
    static let kChooseLanguage = "Please choose one language."
    static let kEmptyMessageDate = "Please enter date and time of message."
    static let kEmptyNoOfAttendess = "Please enter number of attendess."
    static let kEmptyEnglishMessage = "Please enter message in English."
    static let kEmptyArabicMessages = "Please enter message in Arabic."
    static let kEmptyNameOnCard = "Please enter name of card."
    static let kEmptyCardNumber = "Please enter card number."
    static let kEmptyMonth = "Please enter month of expiry date."
    static let kEmptyYear = "Please enter year of expiry date."
    static let kEmptyCVV = "Please enter CVV."
    static let kMinimumPassword = "Please enter atleast 8 characters in password."
    static let kSelectPackage = "Please select package option."
    static let kAcceptTermsCondition = "Please accept terms and conditions."
    
    
}

struct ErrorMessageOfWebApi{
    static let kSomethingWentWrong = "Something went wrong."
}


struct AuthApiMethodName {
    
    static let kSignIn = "organizers/login"
    static let kGet_countries = "organizers/get_countries"
    static let kGet_cities = "organizers/get_cities"
    static let kSignUp = "organizers/sign_up"
    static let kForgetPassword = "organizers/request_new_password"
    static let kGetTermsAndConditions = "General/GetTermsAndConditions"
    static let kSignOut = "organizers/logout"
    static let kVerifyOTP = "organizers/verify_otp"
    static let kResendOTP = "organizers/resend_otp"
    static let kPostContactUs = "Users/ContactUs"
    static let kUpdatePassword = "organizers/update_password"
    static let kVerifyForgotOTP = "organizers/verify_password_code"
}

struct InfluencerApiName {
    static let kVerifyBankAccount = "Users/VerifyBankAccount"
    static let kDepositeToBankAccount = "Users/DepositeToBankAccount"
    static let kGetMoneyEarnedDetails = "Users/GetMoneyEarnedDetails"
    static let kSuggestCharity = "Users/SuggestCharity"
    static let kGetCharityDetails = "Users/GetCharityDetails"
    static let kGetBankDetails = "Users/GetBankDetails"
    static let kAddBankDetails = "Users/AddBankDetails"
    static let kCompleteVideoRequest = "Users/CompleteVideoRequest"
    static let kAddEditCharityDetails = "Users/AddEditCharityDetails"
    static let kGetProfileRates = "Users/GetProfileRates"

    static let kEditProfileRates = "Users/EditProfileRates"
    static let kGetUserLanguages = "Users/GetProfileRatesAndLanguagesAsync"
    static let kEditUserLanguages = "Users/EditProfileRatesAndLanguagesAsync"
    static let kSetCategories = "Users/SetCategories"
    static let kGetUserCategories = "Users/GetUserCategories"
    static let kGetRequestList = "Users/GetRequestList"
    static let kAcceptOrRejectVideoRequest = "Users/AcceptOrRejectVideoRequest"
    
}

struct OrganizerApiName {
    static let kUpcomingEvents = "organizers/user_events"
    static let kCardTypes = "organizers/card_types"
    static let kCard = "organizers/card_type/"
    static let kEventDetails = "organizers/template_details/"
    static let kCreateEvent = "organizers/events"
    static let kAddInvitees = "organizers/events/"
    static let kPlanList = "organizers/plans"
    static let kPlanOfPackageOptions = "organizers/plan_list"
    static let kAddMsgAndColorToEventCards = "organizers/event_cards"
}

struct GuardApiName {
    static let kLoginGuard = "login"
    static let ksecurityEventsAttendee = "security_guards/events/"
    static let kSecurityShowEvent = "security_guards/event"
}

struct AdminApiName {
    
    static let kLoginAdmin = "login"
    static let kEvents = "admins/upcoming_events"
    static let kTicketDetails = "admins/tickets"
}


struct UserApiName{
    
    static let kProfile = "organizers/profile"
    static let kUserInquirySubjects = "organizers/user_inquiry_subjects"
    static let kUserInquiryStatus = "organizers/user_inquiries"
}

struct CustomerApiName {
    static let kGetUserLanguages = "Users/GetUserSelectedLanguages"
    static let kGetUserCardDetails = "Users/GetUserCardDetails"
    static let kGetUserDashboard = "Users/GetUserDashboard"
    static let kGetUserPersonalProfile = "Users/GetUserPersonalProfile"
    static let kSetInfluencersAsFavorite = "Users/SetInfluencersAsFavorite"
    static let kReviewUser = "Users/ReviewUser"
    static let kAddUserCardDetails = "Users/AddUserCardDetails"
    static let kReportUser = "Users/ReportUser"
    static let kEditProfile = "Users/EditProfile"
    static let kVerifyProfile = "Users/UploadVerificationDetails"
    static let kGetUsersFavoriteProfiles = "Users/GetUsersFavoriteProfiles"
    static let kGetUserbyCategoryID = "Users/GetUserbyCategoryID"
    static let kRequestToInfluencer = "Users/RequestToInfluencer"
}


struct UserdefaultsKeyName {
    static let kEventCardID = "event_card_id"
}
