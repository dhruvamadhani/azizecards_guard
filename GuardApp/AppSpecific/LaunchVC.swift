//
//  LaunchVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 30/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import AVKit
class LaunchVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        self.playVideo()

    }
    
     private func playVideo() {
           guard let path = Bundle.main.path(forResource: "splashVideo", ofType:"mp4") else {
               return
           }
           let player = AVPlayer(url: URL(fileURLWithPath: path))
           let playerController = AVPlayerViewController()
           playerController.player = player
           NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
           let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspectFill
           playerLayer.frame = self.view.bounds
           self.view.layer.addSublayer(playerLayer)
           player.play()
       }
       
       @objc func playerDidFinishPlaying(note: NSNotification) {
           print("Video Finished")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                   //
                       let onBoarding = self.storyboard?.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
                       // onBoarding.selectedIndex = 1
                        kAppDelegate.window?.rootViewController = UINavigationController(rootViewController: onBoarding)
                }
       }
    

    

}
