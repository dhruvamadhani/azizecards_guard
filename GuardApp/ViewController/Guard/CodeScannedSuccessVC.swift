//
//  CodeScannedSuccessVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 09/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

protocol CodeSuccessDelegate {
    func addJoinAttendde(_ dicJoinee : [String:Any])
}

class CodeScannedSuccessVC: AppViewController {

    var delegate : CodeSuccessDelegate?
    var arrResult = [String]()
    var dictJoinee = [String:Any]()
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblGuestName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitleView()
      //  self.title = AppHelper.localizedtext(key: "kQRCodeInvite")
        
        print(self.arrResult)
        print(self.arrResult[3] as? String ?? "")
        self.lblGuestName.text = self.arrResult[3] ?? ""
        
        var strEventDate = self.arrResult[2] ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let eventDate = dateFormatter.date(from:strEventDate)
        dateFormatter.dateFormat = "dd/MM/yyyy, hh:mm a"
        let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
        self.lblTime.text = strEventFinalDate
        
        let phoneNumber : String = self.arrResult[4] ?? ""
        self.dictJoinee["name"] = self.lblGuestName.text
        self.dictJoinee["phone_number"] = phoneNumber
        
        let close = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(btnCloseAction(_:)))
        self.navigationItem.rightBarButtonItem  = close
    }
    
    @IBAction func btnScannedContactsActions(_ sender: Any) {
        
    }
    
    @IBAction func btnScannedAnotherCodeAction(_ sender: Any) {
        
        if let delegate = delegate{
            delegate.addJoinAttendde(self.dictJoinee)
            
        }
        self.navigationController?.popViewController(animated: true)
        
    }
    @objc func btnCloseAction(_ sender: Any) {
           //self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
       }

}
