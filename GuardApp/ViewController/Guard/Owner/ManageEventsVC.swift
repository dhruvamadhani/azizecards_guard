//
//  ManageEventsVC.swift
//  AzizECards_Owner
//
//  Created by Dhruva Madhani on 08/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper
import PopupDialog

class ManageEventsVC: AppViewController,UITableViewDelegate,UITableViewDataSource{

    //........MARK: Variables......................
      var arrOngoingEvents = ["Riaz Wedding","ShehnazBirthday Wedding","ShehnazBirthday Wedding","ShehnazBirthday Wedding"]
      var arrCompletedEvents = ["ShehnazBirthday Wedding"]
      var arrTable = [String]()
      let controller = AdminViewModel()
      var type = String()
    var adminObj : adminUser?
    
    var eventList : [EventDetail] = [];
//      var completedEventList : [CompletedEvents] = [];
//      var ongoingEventList : [CompletedEvents] = [];
//      var tblEventList : [CompletedEvents] = [];
      var hasData = Bool()
      var isCallApiFirstTime = Bool()
      //........MARK: IBOutlets......................
    
    @IBOutlet weak var btnCreateEvent: UIButton!
      @IBOutlet weak var viewNoEventFound: UIView!
      @IBOutlet weak var btnLoginOrsignup: UIButton!
      @IBOutlet weak var viewSignupOrLogin: UIView!
    @IBOutlet weak var tblEvents: UITableView!
      @IBOutlet weak var segmentControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
             let fontAttribute = [NSAttributedString.Key.font: Font.tagViewFont,
                                         NSAttributedString.Key.foregroundColor: UIColor.black]
                    segmentControl.setTitleTextAttributes(fontAttribute, for: .normal)
                    
                    let fontAttribute1 = [NSAttributedString.Key.font: Font.tagViewFont,
                                         NSAttributedString.Key.foregroundColor: UIColor.white]
                    segmentControl.setTitleTextAttributes(fontAttribute1, for: .selected)
             if #available(iOS 13.0, *) {
                 segmentControl.selectedSegmentTintColor = COLOR.textColor
             } else {
                 segmentControl.tintColor = COLOR.textColor
                 // Fallback on earlier versions
             }
        

         self.type = "upcoming"
     //   self.title = "Manage Events"
        self.setTitleView()
        
        self.getAdminEvents(type: self.type)
        
       // let logout = UIBarButtonItem(image: UIImage(named: "logout"), style: .plain, target: self, action: #selector(btnlogoutAction))//
        //self.navigationItem.rightBarButtonItem  = logout
        
       
        
        let profile = UIBarButtonItem(image: UIImage(named: "profile"), style: .plain, target: self, action: #selector(btnProfileAction))
        self.navigationItem.rightBarButtonItem  = profile
    }
    //-------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.segmentControl.setTitle(AppHelper.localizedtext(key: "OngoingEvents"), forSegmentAt: 0)
    self.segmentControl.setTitle(AppHelper.localizedtext(key: "CompletedEvents"), forSegmentAt: 1)
    }
    //-------------------------------------------------------
           //MARK: Navigation item action
        //-------------------------------------------------------
    @objc func btnProfileAction(_ sender: Any) {
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "AdminProfileVC") as! AdminProfileVC
               profile.adminObj = self.adminObj
               self.navigationController?.pushViewController(profile, animated: true)
    }
       @objc func btnlogoutAction(_ sender: Any) {
               
               
        let popUp = PopupDialog(title: kAppName, message: AppHelper.localizedtext(key: "logoutMsg"))
               let btnOk = CancelButton(title: "Yes"){
                   self.showActivityIndicatory()
                                 
                          self.controller.logOutApi { (success, response, message, statusCode) in
                                     
                                     print(success)
                                     print(response)
                                     print(message)
                                   //  print(stausCode)
                                     if success{
                                         self.hideActivityIndicator()
                                         DispatchQueue.main.async {
                                             var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
                                                                   // GlobalUtils.getInstance().sessionToken().removeAll()
                                             sessiontoken = ""
                                             GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
                                             
                                             self.showAlertWithCallBack(title: kAppName, message: message) {
                                                 
                                                // self.tabBarController?.selectedIndex = 1
                                              
                                              let login = self.storyboard?.instantiateViewController(withIdentifier: "AdminLoginVC") as! AdminLoginVC
                                              self.navigationItem.setHidesBackButton(true, animated: true)
                                           //   self.hidesBottomBarWhenPushed = true
                                              login.hidesBottomBarWhenPushed = true
                                             // self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                                              self.navigationController?.pushViewController(login, animated: true)

                                             }
                                         }
                                     }else{
                                         self.hideActivityIndicator()
                                         DispatchQueue.main.async {
                                             self.showAlert(message: message, title: kAppName)
                                         }
                                     }
                                 }
                      }
           
               let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                          
                      }
                      popUp.addButtons([btnOk,btnCancel])
                      
                      // Present dialog
                      self.present(popUp, animated: true, completion: nil)
               
               
       //        let alert = UIAlertController(title: kAppName, message: "Do you want to logout?", preferredStyle: .alert)
       //        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
       //            self.showActivityIndicatory()
       //
       //            self.controller.logOutApi { (success, response, message, statusCode) in
       //
       //                       print(success)
       //                       print(response)
       //                       print(message)
       //                     //  print(stausCode)
       //                       if success{
       //                           self.hideActivityIndicator()
       //                           DispatchQueue.main.async {
       //                               var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
       //                                                     // GlobalUtils.getInstance().sessionToken().removeAll()
       //                               sessiontoken = ""
       //                               GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
       //
       //                               self.showAlertWithCallBack(title: kAppName, message: message) {
       //
       //                                  // self.tabBarController?.selectedIndex = 1
       //
       //                                let login = self.storyboard?.instantiateViewController(withIdentifier: "AdminLoginVC") as! AdminLoginVC
       //                                self.navigationItem.setHidesBackButton(true, animated: true)
       //                             //   self.hidesBottomBarWhenPushed = true
       //                                login.hidesBottomBarWhenPushed = true
       //                               // self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       //                                self.navigationController?.pushViewController(login, animated: true)
       //
       //                               }
       //                           }
       //                       }else{
       //                           self.hideActivityIndicator()
       //                           DispatchQueue.main.async {
       //                               self.showAlert(message: message, title: kAppName)
       //                           }
       //                       }
       //                   }
       //        }
       //        let cancelAction = UIAlertAction(title: "No", style: .destructive, handler: nil)
       //        alert.addAction(okAction)
       //        alert.addAction(cancelAction)
       //        self.present(alert, animated: true, completion: nil)
                 
              
              }
 
    
     @IBAction func indexChangedSegmentAction(_ sender: Any) {
            switch self.segmentControl.selectedSegmentIndex{
            case 0 :
                self.type = "upcoming"
                self.isCallApiFirstTime = false
                self.getAdminEvents(type: self.type)
              //  self.callUpcomingEventsApi(self.type)
               
                break
            case 1:
                self.type = "completed"
                self.getAdminEvents(type: self.type)
                self.isCallApiFirstTime = false
              // self.callUpcomingEventsApi(self.type)
                
                break
                
            default:
                break
            }
        }
        
    @IBAction func btnCreateEventAction(_ sender: Any) {
        
    }
    
    @IBAction func btnSignInOrSignupAction(_ sender: Any) {
           
       }
        //--------------------------------------------------------------------
        //MARK: UITableview Datasource
        //--------------------------------------------------------------------
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
            return self.eventList.count
            //return self.ongoingEventList.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OngoingOrCompletedTableCell", for: indexPath)as! OngoingOrCompletedTableCell
            cell.lblEventName.text = self.eventList[indexPath.row].name ?? ""
            
            var img_rightArrow : UIImage = UIImage(named:"rightArrow")!
            let selectedLanguage : String = GlobalUtils.getInstance().getApplicationLanguage()
            if selectedLanguage == "arabic"{
                img_rightArrow = UIImage(named:"left_arrow")!
            }
                   let tintableImage = img_rightArrow.withRenderingMode(.alwaysTemplate)
                   cell.imgRightArrow.image = tintableImage
                   cell.imgRightArrow.tintColor = UIColor(named:"SecondrayColor")
            //cell.lblEventName.text = self.ongoingEventList[indexPath.row].name ?? ""
            return cell
        }
        
        //--------------------------------------------------------------------
        //MARK: UITableview Delegate
        //--------------------------------------------------------------------
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let eventDetails = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailsVC")as! EventDetailsVC
            eventDetails.eventDetailObj = self.eventList[indexPath.row]
            self.navigationController?.pushViewController(eventDetails, animated: true)
            
//            let eventList = self.storyboard?.instantiateViewController(withIdentifier: "OnGoingEventListVC") as! OnGoingEventListVC
//            if self.type == "upcoming"{
//                eventList.isOpenFromOngoing = true
//
//            }else{
//                eventList.isOpenFromOngoing = false
//            }
//            eventList.eventId = self.ongoingEventList[indexPath.row].id ?? 0
//            self.navigationController?.pushViewController(eventList, animated: true)
        }
        //--------------------------------------------------------------------
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 70.0
        }
        
        //--------------------------------------------------------------------
        //MARK: Web api methods
        //--------------------------------------------------------------------
        func callUpcomingEventsApi(_ type : String)  {
            self.showActivityIndicatory()
//            controller.getUpcomingOrCompletdEvents(type: self.type) { (success, response, message, statusCode) in
//                if success{
//                    DispatchQueue.main.async {
//                        print(response)
//                        self.hideActivityIndicator()
//
//                        self.ongoingEventList = response as? [CompletedEvents] ?? []
//                        //self.ongoingEventList.removeAll()
//
//                        if self.isCallApiFirstTime{
//                            if self.ongoingEventList.count != 0 {
//                                self.hasData = true
//                            }
//                            if self.hasData{
//
//                            }else{
//                                if self.ongoingEventList.count == 0{
//                                    self.viewNoEventFound.isHidden = false
//
//                                    self.tblEvents.isHidden = true
//
//                                }else{
//                                    self.viewNoEventFound.isHidden = true
//
//                                    self.tblEvents.isHidden = false
//                                }
//                            }
//                        }else{
//    //                        if self.ongoingEventList.count == 0{
//    //                            self.viewNoEventFound.isHidden = false
//    //
//    //                            self.tblEvents.isHidden = true
//    //
//    //                        }else{
//    //                            self.viewNoEventFound.isHidden = true
//    //
//    //                            self.tblEvents.isHidden = false
//    //                        }
//                        }
//
//
//                        self.tblEvents.reloadData()
//                    }
//                }else{
//                    DispatchQueue.main.async {
//                        self.hideActivityIndicator()
//                        self.showAlert(message: message, title: kAppName)
//                    }
//                }
//            }
        }
    
    
    func getAdminEvents( type : String)  {
        
        
        self.showActivityIndicatory()
        
        controller.adminGetEvents(param: ["type": type]) { (success, eventDetailList, message, statusCode) in
            
            if success{
                self.hideActivityIndicator()
                DispatchQueue.main.async {
                   print(eventDetailList)
                    
                    self.eventList = eventDetailList as? [EventDetail] ?? []
                    
                    self.tblEvents.reloadData()
                }
                     
            }else{
                self.hideActivityIndicator()
                DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
            
            
        }
        
    }
   
}
