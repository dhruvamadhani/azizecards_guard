//
//  CloseTicketDetailVC.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 06/04/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit
import ObjectMapper

extension String {
    func firstCharacterUpperCase() -> String? {
        guard !isEmpty else { return nil }
        let lowerCasedString = self.lowercased()
        return lowerCasedString.replacingCharacters(in: lowerCasedString.startIndex...lowerCasedString.startIndex, with: String(lowerCasedString[lowerCasedString.startIndex]).uppercased())
    }
}


class CloseTicketDetailVC: AppViewController ,UIActionSheetDelegate,CommetDelegate{
 
    func commentSend() {
        let id : Int = self.ticketDetailObj?.id ?? 0
        self.controller.adminShowTicket(ticket_id: "\(id)") { (success, response, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    
                    
                    let ticketObj : TicketsDetail = response as! TicketsDetail
                    if ticketObj != nil {
                        self.setDataOfTicket(ticketObj)
                    }
                    
                    
                                 //  self.showAlert(message: message, title: kAppName)
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
        
    }
    

    
    
    //......................MARK: Variables..................
       
       var ticketDetailObj : TicketsDetail?
    var controller = AdminViewModel()
       var ticket_id = Int()
    var arrComments = [Ticket_Comments]()
       // var userInquiryStatusObj : UserInquiryStatus?
       
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblRequestaCall: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var tblComment : UITableView!
    
       
       //......................MARK: IBOutlets..................
       @IBOutlet weak var lblIssueType: UILabel!
       
       @IBOutlet weak var lblComment: UILabel!
       
       @IBOutlet weak var lblOrganiserName : UILabel!
      
       @IBOutlet weak var btnCloseTicket : UIButton!
       @IBOutlet weak var lblEmail : UILabel!
       @IBOutlet weak var lblPhonEnumber : UILabel!
       
    
    //------------------------------------------------------------------
       //MARK: View life cycle
    //------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.title = "Ticket Details"
        self.setTitleView()

               if self.ticketDetailObj != nil{
                   self.setDataOfTicket(self.ticketDetailObj!)
               }
        let profile = UIBarButtonItem(image: UIImage(named: "Add"), style: .plain, target: self, action: #selector(btnCommentAction))
        self.navigationItem.rightBarButtonItem  = profile
    }
    //-------------------------------------------------------
           //MARK: Navigation item action
        //-------------------------------------------------------
    @objc func btnCommentAction(_ sender: Any) {
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "AddCommentVC") as! AddCommentVC
               profile.ticketDetailObj = self.ticketDetailObj
        profile.delegate = self
               self.navigationController?.pushViewController(profile, animated: true)
    }
    //------------------------------------------------------------------
      func setDataOfTicket(_ ticketDetails : TicketsDetail){
          
          let name : String = ticketDetails.name ?? ""
        self.ticket_id = ticketDetails.id ?? 0
        self.lblStatus.text =  (ticketDetails.status ?? "").firstCharacterUpperCase()
        if name == "" {
            self.lblOrganiserName.text = ""
        }
          self.lblOrganiserName.text = "\(name)"
            let email : String = ticketDetails.email ?? ""
             let phoneNumber : String = ticketDetails.phone_number ?? ""
             
             self.lblEmail.text = email
             self.lblPhonEnumber.text = phoneNumber

          let message : String = ticketDetails.message ?? ""

          let issueType : String = ticketDetails.ticket_type?.name ?? ""
        
        if issueType == "" {
            self.lblIssueType.text = ""
        }else{
            self.lblIssueType.text = "\(issueType)"
        }
        
        let comments : [Ticket_Comments] = ticketDetails.ticket_comments ?? []
        self.arrComments = comments
        if comments.isEmpty == true{
           // self.lblComment.text = ""
        }else{
            
            self.tblComment.reloadData()
            //self.lblComment.text = "\(comments[0].comment_text ?? "")"
        }
        if(ticketDetails.request_a_call == true){
            self.lblRequestaCall.text = AppHelper.localizedtext(key: "kTrue")
        }else{
            self.lblRequestaCall.text = AppHelper.localizedtext(key: "kFalse")
        }
        
        self.lblMessage.text = ticketDetails.message ?? ""
        let organizerObj = ticketDetails.organizer
             let organizer_Fname : String = organizerObj?.first_name ?? ""
             let organizer_Lname : String = organizerObj?.last_name ?? ""
             
              self.lblOrganiserName.text = "\(organizer_Fname) \(organizer_Lname) "
             
             let organizer_phone_number : String = organizerObj?.phone_number ?? ""
             let country_code : String = organizerObj?.country_code ?? ""
             
         self.lblPhonEnumber.text = "\(country_code) \(organizer_phone_number)"
        
    }

   //---------------------------------------------------
     //MARK: UIButton Actions
     //---------------------------------------------------
    @IBAction func btnCloseTicketAction(_ sender: Any) {
        
        var status : String = ""
         // 1
        let optionMenu = UIAlertController(title: kAppName, message: AppHelper.localizedtext(key: "kChooseTicketStatus"), preferredStyle: .actionSheet)
              
          // 2
        
        let inProgressAction = UIAlertAction(title: AppHelper.localizedtext(key: "kInProgress"), style: .default) { (action) in
            status = "in_process"
           // optionMenu.dismiss(animated: true, completion: nil)

            self.showActivityIndicatory()
            
                    let parameters : [String:Any] = ["status":"\(status)"]
                    let params : [String:Any] = ["ticket":parameters]

            self.controller.updateTicketStatus(ticket_id: "\(self.ticket_id)", params: params) { (success, response, message, statusCode) in
                
                self.hideActivityIndicator()
                        if success{
                            DispatchQueue.main.async {
                                self.showAlertWithCallBack(title: kAppName, message: message) {
                                    self.navigationController?.popViewController(animated: true)
                                }
                             //   self.showAlert(message: message, title: kAppName)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.showAlert(message: message, title: kAppName)
                            }
                        }
                    }
        }
         
              
        let resolvedAction = UIAlertAction(title: AppHelper.localizedtext(key: "kResolved"), style: .default) { (action) in
            status = "resolved"

            //
            self.showActivityIndicatory()
                    let parameters : [String:Any] = ["status":"\(status)"]
                    let params : [String:Any] = ["ticket":parameters]

            self.controller.updateTicketStatus(ticket_id: "\(self.ticket_id)", params: params) { (success, response, message, statusCode) in
                self.hideActivityIndicator()
                        if success{
                            DispatchQueue.main.async {
                               self.showAlertWithCallBack(title: kAppName, message: message) {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.showAlert(message: message, title: kAppName)
                            }
                        }
                    }
        }
          // 3
        let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "kCancel"), style: .cancel)
              
          // 4
          optionMenu.addAction(inProgressAction)
          optionMenu.addAction(resolvedAction)
          optionMenu.addAction(cancelAction)
              
          // 5
          self.present(optionMenu, animated: true, completion: nil)
        
        
    }

}

extension CloseTicketDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrComments.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableCell", for: indexPath) as! CommentTableCell
        cell.lblComment.font = Font.tagViewFont
        cell.lblComment.text = self.arrComments[indexPath.row].comment_text ?? ""
        return cell
    }
}




