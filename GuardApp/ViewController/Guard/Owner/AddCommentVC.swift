//
//  AddCommentVC.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 17/04/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit

protocol CommetDelegate {
    func commentSend()
    
}


class AddCommentVC: AppViewController, UITextViewDelegate {

    //......MARK: Variables...................
    var controller = AdminViewModel()
    var ticketDetailObj : TicketsDetail?
    var delegate : CommetDelegate?
    
    //......MARK: IBOutlets...................
    @IBOutlet weak var txtView : UITextView!
    @IBOutlet weak var btnSend : UIButton!
    
    
    //------------------------------------------------------------------
          //MARK: View life cycle
       //------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitleView()
         self.txtView.placeholder = AppHelper.localizedtext(key: "kComments")
        
        //self.txtView.toolbarPlaceholder = AppHelper.localizedtext(key: "kComments")
     
        //self.txtView.pla
    }
    
    

    //------------------------------------------------------------------
    //MARK: Button Action
    //------------------------------------------------------------------
    @IBAction func btnSendAction(_ sender: Any) {
        
        if self.txtView.text == ""{
            
        }else{
            let id : Int = self.ticketDetailObj?.id ?? 0
            print(id)
            self.controller.adminComments(text: self.txtView.text!, event_id: "\(id)") { (success, response, message, statusCode) in
                if success{
                    DispatchQueue.main.async {
                        self.showAlertWithCallBack(title: kAppName, message: message) {
                            
                          //  if let delegate = self.delegate{
                                    self.delegate?.commentSend()
                                self.navigationController?.popViewController(animated: true)
                          //  }
                            
                            
                            
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(message: message, title: kAppName)
                    }
                }
            }
        }
    }

}
