//
//  TicketDetailsVC.swift
//  AzizECards_Owner
//
//  Created by Dhruva Madhani on 08/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper

class TicketDetailsVC: AppViewController {

   //......................MARK: Variables..................
    
    var ticketDetailObj : TicketsDetail?
    
    // var userInquiryStatusObj : UserInquiryStatus?
    
    
    //......................MARK: IBOutlets..................
    @IBOutlet weak var lblIssueType: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblRequestaCall: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblOrganiserName : UILabel!
    @IBOutlet weak var lblResolution : UILabel!
    @IBOutlet weak var imgResolution : UIImageView!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblPhonEnumber : UILabel!
    
    
    
    //------------------------------------------------------------------
    //MARK: View life cycle
    //------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   self.title = "Ticket Details"
        
        self.setTitleView()

        if self.ticketDetailObj != nil{
            self.setDataOfTicket(self.ticketDetailObj!)
        }
        
        
    }
    //------------------------------------------------------------------
    func setDataOfTicket(_ ticketDetails : TicketsDetail){
        
        let name : String = ticketDetails.name ?? ""
        self.lblOrganiserName.text = "\(name)"
    
        let email : String = ticketDetails.email ?? ""
        let phoneNumber : String = ticketDetails.phone_number ?? ""
        
        let organizerObj = ticketDetails.organizer
        let organizer_Fname : String = organizerObj?.first_name ?? ""
        let organizer_Lname : String = organizerObj?.last_name ?? ""
        
         self.lblOrganiserName.text = "\(organizer_Fname) \(organizer_Lname) "
        
        let organizer_phone_number : String = organizerObj?.phone_number ?? ""
        let country_code : String = organizerObj?.country_code ?? ""
        
        
        
        
        //let country_Code : String = ticketDetails.country_code ?? ""
        self.lblStatus.text =  (ticketDetails.status ?? "").firstCharacterUpperCase()
        self.lblEmail.text = email
         self.lblPhonEnumber.text = "\(country_code) \(organizer_phone_number)"
       // self.lblPhonEnumber.text = "\(country_Code) \(phoneNumber)"
        
        let message : String = ticketDetails.message ?? ""

        var issueType : String = ticketDetails.ticket_type?.name ?? ""
        if issueType == ""{
            issueType = ticketDetails.ticket_type?.name ?? ""
        }
        if(ticketDetails.request_a_call == true){
            self.lblRequestaCall.text = AppHelper.localizedtext(key: "kTrue")
        }else{
            self.lblRequestaCall.text = AppHelper.localizedtext(key: "kFalse")
        }
        
        self.lblIssueType.text = "\(issueType)"

        let resolutionStatus : String = (ticketDetails.status ?? "").firstCharacterUpperCase() ?? ""
        self.lblResolution.text = "\(resolutionStatus)"

     

    }

}
