//
//  AdminLoginVC.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 10/01/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit

class AdminLoginVC: AppViewController , UITextFieldDelegate{

    
    
    var controller = AdminViewModel()
    
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitleView()
        //self.title = "Admin Login"
        
    //   self.txtEmail.text = "admin@ecards.com"
     //  self.txtPassword.text = "admin@123"
        
        self.navigationItem.setHidesBackButton(true, animated: false)
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
          self.hidesBottomBarWhenPushed = true
    }
    
    //-------------------------------------------------------
    //MARK: UITextfield delegateMethods
    //-------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    //-------------------------------------------------------
    func textFieldDidEndEditing(_ textField: UITextField) {
              textField.bordrColor = UIColor(named: "lightGray")
    }
    //---------------------------------------------------
      //MARK: UIButton Actions
      //---------------------------------------------------
    @IBAction func btnLoginAction(_ sender: Any) {
        if self.txtEmail.text == "" {
            
        }else if self.txtPassword.text == "" {
            
        }else{
            
            self.showActivityIndicatory()
            controller.adminLogin(Email: self.txtEmail.text!, password: self.txtPassword.text!) { (success, adminUserObj, message, statusCode) in
                
                if success{
                    self.hideActivityIndicator()
                    DispatchQueue.main.async {
                            
                        var currentAdminUser : adminUser?
                        currentAdminUser = adminUserObj as? adminUser
                        
                        var auth_token : String = String()
                        auth_token = currentAdminUser?.auth_token ?? ""
                        GlobalUtils.getInstance().setSessionToken(token: auth_token)
                        
                        let email : String = currentAdminUser?.email ?? ""
                        GlobalUtils.getInstance().setEmail(email: email)
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let tabbarVC = storyboard.instantiateViewController(withIdentifier: "MainTabberController") as! MainTabberController
                        if let vcs = tabbarVC.viewControllers,
                            let nc = vcs.first as? UINavigationController,
                            let home = nc.topViewController as? ManageEventsVC {
                            
                            home.adminObj = currentAdminUser
                            //   pendingOverVC.pendingResult = pendingResult
                        }
                        
                        kAppDelegate.window?.rootViewController = tabbarVC
                        self.tabBarController?.selectedIndex = 0
                        
                    }
                }else{
                    self.hideActivityIndicator()
                    DispatchQueue.main.async {
                        self.showAlert(message: message, title: kAppName)
                    }
                }
                
                
                
            }
            
        }
    }
}
