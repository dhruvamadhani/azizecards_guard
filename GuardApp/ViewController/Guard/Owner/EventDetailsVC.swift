//
//  EventDetailsVC.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 08/01/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit

class EventDetailsVC: AppViewController {

    //......................MARK: Variables..................
       
    var eventDetailObj : EventDetail?
    
     //......................MARK: IBOutlets..................
    @IBOutlet weak var lblOrganiserName: UILabel!
       
       @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblPlanName : UILabel!
    @IBOutlet weak var btnAmount : UIButton!
    @IBOutlet weak var lblDateOfEvent : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var lblInvitationMsgSent : UILabel!
    @IBOutlet weak var lblReminderMsgSent : UILabel!
    @IBOutlet weak var lblCancellationMsgSent : UILabel!
    @IBOutlet weak var lblThankYouMsgSent : UILabel!
    @IBOutlet weak var lblInstantMsgSent : UILabel!
    @IBOutlet weak var lblIsEventCompleted : UILabel!
    
    @IBOutlet weak var discountedView: UIView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
 
    
    
    
    //------------------------------------------------------------------
      //MARK: View life cycle
      //------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

       // self.title = "Event Details"
        
        self.setTitleView()
        
        if self.eventDetailObj != nil{
                   self.setDataOfEvent(self.eventDetailObj!)
               }
               
               
    }
           //------------------------------------------------------------------
           func setDataOfEvent(_ eventDetails : EventDetail){
               
               let name : String = eventDetails.name ?? ""
              // self.lblOrganiserName.text = name
           
            let organizerObj = eventDetails.organizer
            let phoneNumber : String = organizerObj?.phone_number ?? ""
            let country_code : String = organizerObj?.country_code ?? ""
            self.lblPhoneNumber.text = "\(country_code)\(phoneNumber)"
            
            let organizer_Fname : String = organizerObj?.first_name ?? ""
            let organizer_Lname : String = organizerObj?.last_name ?? ""
            
            self.lblOrganiserName.text = "\(organizer_Fname) \(organizer_Lname)"
            

          let location : String = eventDetails.venue ?? ""
            self.lblLocation.text = location
            
               let message : String = eventDetails.message ?? ""

            let planObj  = eventDetails.plan
            
            let plan_name : String = planObj?.name ?? ""
            let plan_amount : Int = planObj?.price ?? 0
            
            self.lblPlanName.text = plan_name
            
            
            let guestObj = eventDetails.guest_plans
            print(guestObj)
            
            let amount : Int = guestObj?[0].amount ?? 0
            
            self.btnAmount.setTitle("\(amount) SR", for: .normal)
            
            var invitation_msg_sent : Bool = eventDetails.invitation_message_deliver ?? false
            var reminder_msg_sent : Bool = eventDetails.reminder_message_deliver ?? false
            var thank_you_Sent : Bool = eventDetails.thankyou_message_deliver ?? false
            var cancel_msg_sent : Bool = eventDetails.cancelation_message_deliver ?? false
            var instant_msg_sent : Bool = eventDetails.instant_invitation_sent ?? false
            var is_event_complete : Bool = eventDetails.is_completed ?? false
            
            if invitation_msg_sent == true || reminder_msg_sent == true || cancel_msg_sent == true || thank_you_Sent == true || instant_msg_sent == true || is_event_complete == true{
                self.lblInvitationMsgSent.text = AppHelper.localizedtext(key: "kTrue")
                self.lblReminderMsgSent.text  = AppHelper.localizedtext(key: "kTrue")
                self.lblThankYouMsgSent.text =  AppHelper.localizedtext(key: "kTrue")
                 self.lblCancellationMsgSent.text = AppHelper.localizedtext(key: "kTrue")
                self.lblInstantMsgSent.text = AppHelper.localizedtext(key: "kTrue")
                self.lblIsEventCompleted.text = AppHelper.localizedtext(key: "kTrue")
            }else{
                self.lblInvitationMsgSent.text = AppHelper.localizedtext(key: "kFalse")
                self.lblReminderMsgSent.text  = AppHelper.localizedtext(key: "kFalse")
                self.lblThankYouMsgSent.text =  AppHelper.localizedtext(key: "kFalse")
                 self.lblCancellationMsgSent.text = AppHelper.localizedtext(key: "kFalse")
               self.lblInstantMsgSent.text = AppHelper.localizedtext(key: "kFalse")
                self.lblIsEventCompleted.text = AppHelper.localizedtext(key: "kFalse")
                
            }
            
            if invitation_msg_sent == true{
                 self.lblInvitationMsgSent.text = AppHelper.localizedtext(key: "kTrue")
            }else{
                self.lblInvitationMsgSent.text = AppHelper.localizedtext(key: "kFalse")
            }
            
            if reminder_msg_sent == true{
                self.lblReminderMsgSent.text  = AppHelper.localizedtext(key: "kTrue")
            }else{
                self.lblReminderMsgSent.text  = AppHelper.localizedtext(key: "kFalse")
            }

            if cancel_msg_sent == true{
                self.lblCancellationMsgSent.text = AppHelper.localizedtext(key: "kTrue")
            }else{
                self.lblCancellationMsgSent.text = AppHelper.localizedtext(key: "kFalse")
            }
            
            if thank_you_Sent == true{
                self.lblThankYouMsgSent.text =  AppHelper.localizedtext(key: "kTrue")
            }else{
                self.lblThankYouMsgSent.text =  AppHelper.localizedtext(key: "kFalse")
            }
            
            if instant_msg_sent == true{
                self.lblInstantMsgSent.text = AppHelper.localizedtext(key: "kTrue")
            }else{
                self.lblInstantMsgSent.text = AppHelper.localizedtext(key: "kFalse")
            }
            
            if is_event_complete == true{
                self.lblIsEventCompleted.text = AppHelper.localizedtext(key: "kTrue")
            }else{
                self.lblIsEventCompleted.text = AppHelper.localizedtext(key: "kFalse")
            }
            
//            self.lblInvitationMsgSent.text = "\(invitation_msg_sent)"
//            self.lblReminderMsgSent.text = "\(reminder_msg_sent)"
//            self.lblThankYouMsgSent.text = "\(thank_you_Sent)"
//            self.lblCancellationMsgSent.text = "\(cancel_msg_sent)"
//            self.lblInstantMsgSent.text = "\(instant_msg_sent)"
            
            let arrPayments = eventDetails.payments ?? []
            if(arrPayments.count>0){
                let dictData = arrPayments[0]
                self.discountedView.isHidden = false
               
                self.lblSubTotal.text = "\(dictData.actual_amount as? String ?? "") SR"
                self.lblDiscount.text = "\(dictData.discount as? String ?? "") SR"
                let amount = Int(dictData.amount as? String ?? "") ?? 0
                let total = amount / 100
                self.lblTotalAmount.text = "\(total) SR"
            }else{
                self.discountedView.isHidden = true
               
            }
            
            
            
            var strEventDate = eventDetails.event_date ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            let eventDate = dateFormatter.date(from:strEventDate)
            dateFormatter.dateFormat = "dd/MM/yyyy, hh:mm a"
            let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
            self.lblDateOfEvent.text = strEventFinalDate
            
            
            
            let payment = eventDetails.payments
//            let discount : String = payment.discount ?? ""
//            print(discount)
            
            
           }
    
    @IBAction func btnAmountAction(_ sender: Any) {
        
    }

}
