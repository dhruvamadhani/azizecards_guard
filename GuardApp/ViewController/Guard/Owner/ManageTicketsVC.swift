//
//  ManageTicketsVC.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 08/01/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit
import PopupDialog

class ManageTicketsVC: AppViewController, UITableViewDataSource, UITableViewDelegate {

    //........MARK: Variables......................
      var arrOngoingEvents = ["Riaz Wedding","ShehnazBirthday Wedding","ShehnazBirthday Wedding","ShehnazBirthday Wedding"]
      var arrCompletedEvents = ["ShehnazBirthday Wedding"]
      var arrTable = [String]()
      let controller = AdminViewModel()
      var type = String()
    var ticketList : [TicketsDetail] = [];
//      var completedEventList : [CompletedEvents] = [];
//      var ongoingEventList : [CompletedEvents] = [];
//      var tblEventList : [CompletedEvents] = [];
      var hasData = Bool()
      var isCallApiFirstTime = Bool()
    var adminObj : adminUser?
      //........MARK: IBOutlets......................
    
    @IBOutlet weak var btnCreateEvent: UIButton!
      @IBOutlet weak var viewNoEventFound: UIView!
      @IBOutlet weak var btnLoginOrsignup: UIButton!
      @IBOutlet weak var viewSignupOrLogin: UIView!
    @IBOutlet weak var tblTickets: UITableView!
      @IBOutlet weak var segmentControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
             let fontAttribute = [NSAttributedString.Key.font: Font.tagViewFont,
                                         NSAttributedString.Key.foregroundColor: UIColor.black]
                    segmentControl.setTitleTextAttributes(fontAttribute, for: .normal)
                    
                    let fontAttribute1 = [NSAttributedString.Key.font: Font.tagViewFont,
                                         NSAttributedString.Key.foregroundColor: UIColor.white]
                    segmentControl.setTitleTextAttributes(fontAttribute1, for: .selected)
             if #available(iOS 13.0, *) {
                 segmentControl.selectedSegmentTintColor = COLOR.textColor
             } else {
                 segmentControl.tintColor = COLOR.textColor
                 // Fallback on earlier versions
             }
        
       

        
         self.type = "open"
        //      self.title = "Manage Tickets"
        
       self.setTitleView()
        
//        let logout = UIBarButtonItem(image: UIImage(named: "logout"), style: .plain, target: self, action: #selector(btnlogoutAction))
//        self.navigationItem.rightBarButtonItem  = logout
        let profile = UIBarButtonItem(image: UIImage(named: "profile"), style: .plain, target: self, action: #selector(btnProfileAction))
             self.navigationItem.rightBarButtonItem  = profile
        
        
    }
    //-------------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getAdminTickets(type: self.type)
        
            self.segmentControl.setTitle(AppHelper.localizedtext(key: "OpenTickets"), forSegmentAt: 0)
             self.segmentControl.setTitle(AppHelper.localizedtext(key: "CloseTickets"), forSegmentAt: 1)
        
        
    }
    //-------------------------------------------------------
           //MARK: Navigation item action
        //-------------------------------------------------------
    @objc func btnProfileAction(_ sender: Any) {
           let profile = self.storyboard?.instantiateViewController(withIdentifier: "AdminProfileVC") as! AdminProfileVC
                  profile.adminObj = self.adminObj
                  self.navigationController?.pushViewController(profile, animated: true)
       }
       @objc func btnlogoutAction(_ sender: Any) {
        
        
        let popUp = PopupDialog(title: kAppName, message: AppHelper.localizedtext(key: "logoutMsg"))
        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")){
            self.showActivityIndicatory()
                          
                   self.controller.logOutApi { (success, response, message, statusCode) in
                              
                              print(success)
                              print(response)
                              print(message)
                            //  print(stausCode)
                              if success{
                                  self.hideActivityIndicator()
                                  DispatchQueue.main.async {
                                      var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
                                                            // GlobalUtils.getInstance().sessionToken().removeAll()
                                      sessiontoken = ""
                                      GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
                                      
                                      self.showAlertWithCallBack(title: kAppName, message: message) {
                                          
                                         // self.tabBarController?.selectedIndex = 1
                                       
                                       let login = self.storyboard?.instantiateViewController(withIdentifier: "AdminLoginVC") as! AdminLoginVC
                                       self.navigationItem.setHidesBackButton(true, animated: true)
                                    //   self.hidesBottomBarWhenPushed = true
                                       login.hidesBottomBarWhenPushed = true
                                      // self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                                       self.navigationController?.pushViewController(login, animated: true)

                                      }
                                  }
                              }else{
                                  self.hideActivityIndicator()
                                  DispatchQueue.main.async {
                                      self.showAlert(message: message, title: kAppName)
                                  }
                              }
                          }
               }
    
        let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                   
               }
               popUp.addButtons([btnOk,btnCancel])
               
               // Present dialog
               self.present(popUp, animated: true, completion: nil)
        
        
//        let alert = UIAlertController(title: kAppName, message: "Do you want to logout?", preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
//            self.showActivityIndicatory()
//                   
//            self.controller.logOutApi { (success, response, message, statusCode) in
//                       
//                       print(success)
//                       print(response)
//                       print(message)
//                     //  print(stausCode)
//                       if success{
//                           self.hideActivityIndicator()
//                           DispatchQueue.main.async {
//                               var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
//                                                     // GlobalUtils.getInstance().sessionToken().removeAll()
//                               sessiontoken = ""
//                               GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
//                               
//                               self.showAlertWithCallBack(title: kAppName, message: message) {
//                                   
//                                  // self.tabBarController?.selectedIndex = 1
//                                
//                                let login = self.storyboard?.instantiateViewController(withIdentifier: "AdminLoginVC") as! AdminLoginVC
//                                self.navigationItem.setHidesBackButton(true, animated: true)
//                             //   self.hidesBottomBarWhenPushed = true
//                                login.hidesBottomBarWhenPushed = true
//                               // self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//                                self.navigationController?.pushViewController(login, animated: true)
//
//                               }
//                           }
//                       }else{
//                           self.hideActivityIndicator()
//                           DispatchQueue.main.async {
//                               self.showAlert(message: message, title: kAppName)
//                           }
//                       }
//                   }
//        }
//        let cancelAction = UIAlertAction(title: "No", style: .destructive, handler: nil)
//        alert.addAction(okAction)
//        alert.addAction(cancelAction)
//        self.present(alert, animated: true, completion: nil)
          
       
       }
 
    
     @IBAction func indexChangedSegmentAction(_ sender: Any) {
            switch self.segmentControl.selectedSegmentIndex{
            case 0 :
                self.type = "open"
                self.isCallApiFirstTime = false
                self.getAdminTickets(type: self.type)
              //  self.callUpcomingEventsApi(self.type)
               
                break
            case 1:
                self.type = "closed"
                self.isCallApiFirstTime = false
                self.getAdminTickets(type: self.type)
              // self.callUpcomingEventsApi(self.type)
                
                break
                
            default:
                break
            }
        }
        
    @IBAction func btnCreateEventAction(_ sender: Any) {
        
    }
    
    @IBAction func btnSignInOrSignupAction(_ sender: Any) {
           
       }
        //--------------------------------------------------------------------
        //MARK: UITableview Datasource
        //--------------------------------------------------------------------
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
            return self.ticketList.count
            //return self.ongoingEventList.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OngoingOrCompletedTableCell", for: indexPath)as! OngoingOrCompletedTableCell
             cell.lblEventName.text = self.ticketList[indexPath.row].name ?? ""
            var img_rightArrow : UIImage = UIImage(named:"rightArrow")!
            let selectedLanguage : String = GlobalUtils.getInstance().getApplicationLanguage()
                       if selectedLanguage == "arabic"{
                           img_rightArrow = UIImage(named:"left_arrow")!
                       }
                   let tintableImage = img_rightArrow.withRenderingMode(.alwaysTemplate)
                   cell.imgRightArrow.image = tintableImage
                   cell.imgRightArrow.tintColor = UIColor(named:"SecondrayColor")
            let status : String = self.ticketList[indexPath.row].status ?? ""
            
            let Status_i : Int = self.ticketList[indexPath.row].id ?? 0
            
            
            
            //if Status_i == 1{
            //AppHelper.localizedtext(key: "kInProgressGreen") as? String ?? ""{
            if status == AppHelper.localizedtext(key: "kInProgressGreen") as? String ?? ""{
                
                cell.viewGreen.isHidden = false
            }else{
                cell.viewGreen.isHidden = true
            }
            
            //cell.lblEventName.text = self.arrOngoingEvents[indexPath.row] ?? ""
            return cell
        }
        
        //--------------------------------------------------------------------
        //MARK: UITableview Delegate
        //--------------------------------------------------------------------
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if self.segmentControl.selectedSegmentIndex == 0 {
                let ticketDetails = self.storyboard?.instantiateViewController(withIdentifier: "CloseTicketDetailVC")as! CloseTicketDetailVC
                 ticketDetails.ticketDetailObj = self.ticketList[indexPath.row]
                self.navigationController?.pushViewController(ticketDetails, animated: true)
            }else{
                let ticketDetails = self.storyboard?.instantiateViewController(withIdentifier: "TicketDetailsVC")as! TicketDetailsVC
                ticketDetails.ticketDetailObj = self.ticketList[indexPath.row]
                self.navigationController?.pushViewController(ticketDetails, animated: true)
            }
            
            
           
            
//            let eventList = self.storyboard?.instantiateViewController(withIdentifier: "OnGoingEventListVC") as! OnGoingEventListVC
//            if self.type == "upcoming"{
//                eventList.isOpenFromOngoing = true
//
//            }else{
//                eventList.isOpenFromOngoing = false
//            }
//            eventList.eventId = self.ongoingEventList[indexPath.row].id ?? 0
//            self.navigationController?.pushViewController(eventList, animated: true)
        }
        //--------------------------------------------------------------------
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 70.0
        }
        
        //--------------------------------------------------------------------
        //MARK: Web api methods
        //--------------------------------------------------------------------
        func callUpcomingEventsApi(_ type : String)  {
            self.showActivityIndicatory()
//            controller.getUpcomingOrCompletdEvents(type: self.type) { (success, response, message, statusCode) in
//                if success{
//                    DispatchQueue.main.async {
//                        print(response)
//                        self.hideActivityIndicator()
//
//                        self.ongoingEventList = response as? [CompletedEvents] ?? []
//                        //self.ongoingEventList.removeAll()
//
//                        if self.isCallApiFirstTime{
//                            if self.ongoingEventList.count != 0 {
//                                self.hasData = true
//                            }
//                            if self.hasData{
//
//                            }else{
//                                if self.ongoingEventList.count == 0{
//                                    self.viewNoEventFound.isHidden = false
//
//                                    self.tblEvents.isHidden = true
//
//                                }else{
//                                    self.viewNoEventFound.isHidden = true
//
//                                    self.tblEvents.isHidden = false
//                                }
//                            }
//                        }else{
//    //                        if self.ongoingEventList.count == 0{
//    //                            self.viewNoEventFound.isHidden = false
//    //
//    //                            self.tblEvents.isHidden = true
//    //
//    //                        }else{
//    //                            self.viewNoEventFound.isHidden = true
//    //
//    //                            self.tblEvents.isHidden = false
//    //                        }
//                        }
//
//
//                        self.tblEvents.reloadData()
//                    }
//                }else{
//                    DispatchQueue.main.async {
//                        self.hideActivityIndicator()
//                        self.showAlert(message: message, title: kAppName)
//                    }
//                }
//            }
        }
    
    
    
    func getAdminTickets( type : String)  {
         
         
         self.showActivityIndicatory()
         
         controller.adminGetTicketsEvents(param: ["type": type]) { (success, ticketsDetailList, message, statusCode) in
             
             if success{
                 self.hideActivityIndicator()
                 DispatchQueue.main.async {
                    print(ticketsDetailList)
                     
                     self.ticketList = ticketsDetailList as? [TicketsDetail] ?? []
                     
                    if self.ticketList.count == 0 {
                        self.viewNoEventFound.isHidden = false
                    
                    }else{
                        self.viewNoEventFound.isHidden = true
                    }
                    
                     self.tblTickets.reloadData()
                 }
                      
             }else{
                 self.hideActivityIndicator()
                 DispatchQueue.main.async {
                     self.showAlert(message: message, title: kAppName)
                 }
             }
             
             
         }
         
     }
   
}
