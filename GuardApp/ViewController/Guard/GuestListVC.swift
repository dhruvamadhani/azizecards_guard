//
//  GuestListVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 09/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class GuestListVC: AppViewController, UITableViewDataSource , UITableViewDelegate {
   
    

    var totalCount = Int()
    var isOpenFromScan = Bool()
    var arrJoin = [[String:Any]]()
    @IBOutlet weak var lblScannedGuestNumber: UILabel!
    @IBOutlet weak var tblGuests: UITableView!
    @IBOutlet weak var lblStaticScan : UILabel!
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitleView()
        print(arrJoin)
        if self.isOpenFromScan{
            self.lblStaticScan.text = AppHelper.localizedtext(key: "kScanned")
        }else{
            self.lblStaticScan.text = AppHelper.localizedtext(key: "kNotScanned")
        }
        
        self.lblScannedGuestNumber.text = "\(self.arrJoin.count)/\(self.totalCount)"
    }
    
    //---------------------------------------------------
        //MARK: Tableview Datasource
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrJoin.count
    }
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuestListTableCell", for: indexPath) as! GuestListTableCell
        cell.imgSucess.image = UIImage(named: "success")
        let dictData : [String:Any] = self.arrJoin[indexPath.row] as? [String:Any] ?? [:]
        if dictData.isEmpty{
            cell.lblGuestName.text = ""
        }else{
            let name : String = dictData["full_name"] as? String ?? ""
            let phoneNumber : String = dictData["phone_number"] as? String ?? ""
            cell.lblGuestName.text = "\(name)  \(phoneNumber)"
            
            
        }
        cell.lblTime.text = ""
        
        
        
        return cell
        
    }
    
    //---------------------------------------------------
    //MARK: Tableview Delegate
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}
