//
//  GuardProfileVC.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 08/04/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit
import ObjectMapper
import PopupDialog


class GuardProfileVC: AppViewController {

    //.....MARK:Variables..........................
     let controller = GuardViewModel()
    var guardObj : Guard?
    
    //........MARK: IBOutlet.................................
    @IBOutlet weak var lblStaticName : UILabel!
    @IBOutlet weak var lblStaticMobileNumber : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblMobileNumber : UILabel!
    @IBOutlet weak var btnLanguageSelection : UIButton!
    @IBOutlet weak var btnLogout : UIButton!
    
    //--------------------------------------------------------
    //MARK: View Life Cycle
    //--------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitleView()
        
          let imgLogout = UIImage(named: "logout")
        self.btnLogout.setImage(imgLogout?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnLogout.tintColor = UIColor(named: "backgroundColor")
        
        if self.guardObj != nil{
            self.setData(guardObj: self.guardObj!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let selecetdLanguage : String = GlobalUtils.getInstance().getApplicationLanguage()
        if selecetdLanguage == "arabic"{
            self.lblStaticName.text = "اسم"
            self.lblStaticMobileNumber.text = "رقم الهاتف المحمول"
            self.btnLogout.setTitle("تسجيل خروج", for: .normal)
            self.btnLanguageSelection.setTitle("اختيار اللغة", for: .normal)
            self.lblMobileNumber.textAlignment = .right
            self.lblName.textAlignment = .right
            
            
            self.btnLogout.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 20.0)
           
        }else{
            self.lblStaticName.text = "Name"
            self.lblStaticMobileNumber.text = "Mobile Number"
            self.btnLogout.setTitle("Logout", for: .normal)
            self.btnLanguageSelection.setTitle("Language Selection", for: .normal)
            self.lblMobileNumber.textAlignment = .left
            self.lblName.textAlignment = .left
            self.btnLogout.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 0.0)
        }
    }
    //---------------------------------------------------
    //MARK: Custom Function
    //---------------------------------------------------
    func setData(guardObj : Guard){
        
        let name : String = guardObj.name ?? ""
        let phoneNumber : String = guardObj.phone_number ?? ""
        
        self.lblName.text = "\(name)"
        self.lblMobileNumber.text = "\(phoneNumber)"
    }
   //--------------------------------------------------------
     //MARK: IBAction of UIButton
    //--------------------------------------------------------
    @IBAction func btnLanguageSelectionAction(_ sender: Any) {
        
        let languageSelection = self.storyboard?.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
        languageSelection.isOpenFromProfile = true
        self.navigationController?.pushViewController(languageSelection, animated: true)
        
    }
    //--------------------------------------------------------
    @IBAction func btnLogoutAction(_ sender: Any) {
            
            
        let popUp = PopupDialog(title: kAppName, message: AppHelper.localizedtext(key: "logoutMsg"))
        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")){
                self.showActivityIndicatory()
                              
                       self.controller.logOutApi { (success, response, message, statusCode) in
                                  
                                  print(success)
                                  print(response)
                                  print(message)
                                //  print(stausCode)
                                  if success{
                                      self.hideActivityIndicator()
                                      DispatchQueue.main.async {
                                          var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
                                                                // GlobalUtils.getInstance().sessionToken().removeAll()
                                          sessiontoken = ""
                                          GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
                                          
                                          self.showAlertWithCallBack(title: kAppName, message: message) {
                                              
                                             // self.tabBarController?.selectedIndex = 1
                                           
                                           let login = self.storyboard?.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
                                           self.navigationItem.setHidesBackButton(true, animated: true)
                                        //   self.hidesBottomBarWhenPushed = true
                                           login.hidesBottomBarWhenPushed = true
                                          // self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                                           self.navigationController?.pushViewController(login, animated: true)

                                          }
                                      }
                                  }else{
                                      self.hideActivityIndicator()
                                      DispatchQueue.main.async {
                                          self.showAlert(message: message, title: kAppName)
                                      }
                                  }
                              }
                   }
        
            let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                       
                   }
                   popUp.addButtons([btnOk,btnCancel])
                   
                   // Present dialog
                   self.present(popUp, animated: true, completion: nil)
      
    }
    //--------------------------------------------------------
}
