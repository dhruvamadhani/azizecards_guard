//
//  QRScanVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 10/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
//import SwiftQRCode
import QRCodeReader
import ObjectMapper
import AVFoundation
import PopupDialog

class QRScanVC: AppViewController , QRCodeReaderViewControllerDelegate, CodeSuccessDelegate{
    
    
   
    
    //--------------------------------------------------------
    //MARK: CodeSuccessDelegate
    //--------------------------------------------------------
    func addJoinAttendde(_ dicJoinee: [String : Any]) {
        
      let dicSimilarContact = self.arrJoinedAttendees.filter{ ($0["phone_number"] as? String ?? "").contains(dicJoinee["phone_number"] as? String ?? "") }
        if dicSimilarContact.isEmpty{
            self.arrJoinedAttendees.append(dicJoinee)
        }else{
            self.showAlert(message: AppHelper.localizedtext(key: "kSameContacts"), title: kAppName)
        }
        print(dicJoinee)
       // self.arrJoinedAttendees.append(dicJoinee)
         let ratio : Float = Float(self.arrJoinedAttendees.count)/Float(self.totalCount)
       let count = self.arrJoinedAttendees.count + 1
        self.progressView.progress = ratio
        self.lblScanned.text = "\(AppHelper.localizedtext(key: "kScanned")) \(count)"
    }
    
    
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
      reader.stopScanning()

      dismiss(animated: true) { [weak self] in
        let alert = UIAlertController(
          title: "QRCodeReader",
          message: String (format:"%@ (of type %@)", result.value, result.metadataType),
          preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))

        self?.present(alert, animated: true, completion: nil)
      }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
      reader.stopScanning()

      dismiss(animated: true, completion: nil)
    }
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
      print("Switching capture to: \(newCaptureDevice.device.localizedName)")
    }

   

     //MARK: Variables.........................
//    let scanner = QRCode()
    var event_date = String()
    var event_name = String()
    let controller = GuardViewModel()
    var guardObj : Guard?
    var eventDetailObj :EventDetail?
    var arrJoin = [[String:Any]]()
    var arrRemaining_attendees = [[String:Any]]()
    var totalCount = Int()
    var arrJoinedAttendees = [[String:Any]]()
    @IBOutlet weak var previewView: QRCodeReaderView! {
      didSet {
        previewView.setupComponents(with: QRCodeReaderViewControllerBuilder {
          $0.reader                 = reader
          $0.showTorchButton        = false
          $0.showSwitchCameraButton = false
          $0.showCancelButton       = false
          $0.showOverlayView        = true
          $0.rectOfInterest         = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        })
      }
    }
    lazy var reader: QRCodeReader = QRCodeReader()
     lazy var readerVC: QRCodeReaderViewController = {
       let builder = QRCodeReaderViewControllerBuilder {
         $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
         $0.showTorchButton         = true
         $0.preferredStatusBarStyle = .lightContent
         $0.showOverlayView         = true
         $0.rectOfInterest          = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
           
         $0.reader.stopScanningWhenCodeIsFound = true
       }
      
       return QRCodeReaderViewController(builder: builder)
     }()
    
    //MARK: IBOutlets.........................
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var btnNotScanned: UIButton!
    @IBOutlet weak var lblScanned: UILabel!
    @IBOutlet weak var btnScanQRCode: UIButton!
    @IBOutlet weak var btnScanned: UIButton!
    @IBOutlet weak var lblNotScanned: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var viewScan: UIView!
    @IBOutlet weak var btnScan : UIButton!
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitleView()
        print(guardObj)
        
         let imgCirlce = UIImage(named: "Circle_Gray")

        self.btnScanned.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnScanned.tintColor = UIColor(named: "SecondrayColor")
        
        self.btnNotScanned.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnNotScanned.tintColor = UIColor(named: "lightGray")
        
        self.title = AppHelper.localizedtext(key: "kScanCode")
        
        let profile = UIBarButtonItem(image: UIImage(named: "profile"), style: .plain, target: self, action: #selector(btnProfileAction(_:)))
        self.navigationItem.rightBarButtonItem  = profile
        
            //   let event_name : String = event_name
        self.lblEventName.text = event_name
        self.lblEventDate.text = event_date
        
     
        
        self.callGetShowEventApi()
        
        self.readerVC.delegate = self
    }
    
    //---------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        self.navigationItem.setHidesBackButton(true,animated:false)
        
        let event_id : Int = guardObj?.event_id ?? 0
        
        
        self.callGetSecurityEventsAttendee("\(event_id)")
        
        guard checkScanPermissions(), !reader.isRunning else { return }
        self.reader.stopScanningWhenCodeIsFound = true
        reader.startScanning()
               reader.didFindCode = { result in
                self.reader.stopScanningWhenCodeIsFound = true
                 print("Completion with result: \(result.value) of type \(result.metadataType)")
                self.readerVC.stopScanning()
                var str = result.value as? String ?? ""
                var arrStr = str.components(separatedBy : ",")
                var dictObj = self.convertToDictionary(text: str) as? [String:Any] ?? [:]
                
                let name : String = arrStr[3] ?? ""
                let phoneNumber : String = arrStr[4] ?? ""
                
                print(name)
                print(phoneNumber)
                
                let message1 : String = AppHelper.localizedtext(key: "kScanSuccess")
                var finalMessage : String = "\(message1) \n \(name) & \(phoneNumber)"
                print(finalMessage)
                
                finalMessage = finalMessage.replacingOccurrences(of: "}", with: "")
                
                
                var token : String = arrStr[0] ?? ""
                print(token)
                token = token.replacingOccurrences(of: "}", with: "")
                token = token.replacingOccurrences(of: "{token: ", with: "")
                var arrToken : [String] = [token]
                let params : [String:Any] = ["tokens":arrToken]
                let event_id : Int = self.guardObj?.event_id ?? 0
                self.controller.checkQRCode(ticket_id: "\(event_id)", params: params) { (success, response, message, statusCode) in
                    
                    if success{
                        DispatchQueue.main.async {
                            
                            self.showAlertWithCallBack(title: kAppName, message: "\(finalMessage)") {
                                let successVc = self.storyboard?.instantiateViewController(withIdentifier: "CodeScannedSuccessVC") as! CodeScannedSuccessVC
                                successVc.arrResult = arrStr
                                 successVc.delegate = self
                                 self.navigationController?.pushViewController(successVc, animated: true)
                            }
                           
                        }
                    }else{
                        DispatchQueue.main.async {
                            
                            if statusCode == 422 {
                               // self.reader.dis
                                self.reader.stopScanning()
                            }else{
                                self.showAlertWithCallBack(title: kAppName, message: message){
                                
                                    let failVC = self.storyboard?.instantiateViewController(withIdentifier: "QRScanErrorVC") as! QRScanErrorVC
                                    self.navigationController?.pushViewController(failVC, animated: true)
                                }
                            }
                            
                        }
                    }
                }
                
//                self.showAlertWithCallBack(title: kAppName, message: "\(finalMessage)") {
//
////                  let count = self.arrJoinedAttendees.count + 1
////                  self.progressView.progress = Float(count)
////                    self.lblScanned.text = "Scanned \(count)"
//
//
//                    let successVc = self.storyboard?.instantiateViewController(withIdentifier: "CodeScannedSuccessVC") as! CodeScannedSuccessVC
//                   successVc.arrResult = arrStr
//                    successVc.delegate = self
//                    self.navigationController?.pushViewController(successVc, animated: true)
//                }
             
        }
        
        reader.didFailDecoding = {
                
                self.showAlertWithCallBack(title: kAppName, message: "Failed to scan") {
                    
                    let failVC = self.storyboard?.instantiateViewController(withIdentifier: "QRScanErrorVC") as! QRScanErrorVC
                    self.navigationController?.pushViewController(failVC, animated: true)
                }
        }
    }
    
    @objc func btnProfileAction(_ sender:Any) {
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "GuardProfileVC") as! GuardProfileVC
        profile.guardObj = self.guardObj
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    
    @objc func btnLogoutAction(_ sender: Any) {
            
            
        let popUp = PopupDialog(title: kAppName, message: AppHelper.localizedtext(key: "logoutMsg"))
            let btnOk = CancelButton(title: "Yes"){
                self.showActivityIndicatory()
                              
                       self.controller.logOutApi { (success, response, message, statusCode) in
                                  
                                  print(success)
                                  print(response)
                                  print(message)
                                //  print(stausCode)
                                  if success{
                                      self.hideActivityIndicator()
                                      DispatchQueue.main.async {
                                          var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
                                                                // GlobalUtils.getInstance().sessionToken().removeAll()
                                          sessiontoken = ""
                                          GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
                                          
                                          self.showAlertWithCallBack(title: kAppName, message: message) {
                                              
                                             // self.tabBarController?.selectedIndex = 1
                                           
                                           let login = self.storyboard?.instantiateViewController(withIdentifier: "GuardLoginVC") as! GuardLoginVC
                                           self.navigationItem.setHidesBackButton(true, animated: true)
                                        //   self.hidesBottomBarWhenPushed = true
                                           login.hidesBottomBarWhenPushed = true
                                          // self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                                           self.navigationController?.pushViewController(login, animated: true)

                                          }
                                      }
                                  }else{
                                      self.hideActivityIndicator()
                                      DispatchQueue.main.async {
                                          self.showAlert(message: message, title: kAppName)
                                      }
                                  }
                              }
                   }
        
            let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                       
                   }
                   popUp.addButtons([btnOk,btnCancel])
                   
                   // Present dialog
                   self.present(popUp, animated: true, completion: nil)
            
            
    //        let alert = UIAlertController(title: kAppName, message: "Do you want to logout?", preferredStyle: .alert)
    //        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
    //            self.showActivityIndicatory()
    //
    //            self.controller.logOutApi { (success, response, message, statusCode) in
    //
    //                       print(success)
    //                       print(response)
    //                       print(message)
    //                     //  print(stausCode)
    //                       if success{
    //                           self.hideActivityIndicator()
    //                           DispatchQueue.main.async {
    //                               var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
    //                                                     // GlobalUtils.getInstance().sessionToken().removeAll()
    //                               sessiontoken = ""
    //                               GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
    //
    //                               self.showAlertWithCallBack(title: kAppName, message: message) {
    //
    //                                  // self.tabBarController?.selectedIndex = 1
    //
    //                                let login = self.storyboard?.instantiateViewController(withIdentifier: "AdminLoginVC") as! AdminLoginVC
    //                                self.navigationItem.setHidesBackButton(true, animated: true)
    //                             //   self.hidesBottomBarWhenPushed = true
    //                                login.hidesBottomBarWhenPushed = true
    //                               // self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    //                                self.navigationController?.pushViewController(login, animated: true)
    //
    //                               }
    //                           }
    //                       }else{
    //                           self.hideActivityIndicator()
    //                           DispatchQueue.main.async {
    //                               self.showAlert(message: message, title: kAppName)
    //                           }
    //                       }
    //                   }
    //        }
    //        let cancelAction = UIAlertAction(title: "No", style: .destructive, handler: nil)
    //        alert.addAction(okAction)
    //        alert.addAction(cancelAction)
    //        self.present(alert, animated: true, completion: nil)
              
           
           }

    private func checkScanPermissions() -> Bool {
      do {
        return try QRCodeReader.supportsMetadataObjectTypes()
      } catch let error as NSError {
        let alert: UIAlertController

        switch error.code {
        case -11852:
          alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)

          alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
            DispatchQueue.main.async {
              if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.openURL(settingsURL)
              }
            }
          }))

          alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        default:
          alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        }

        present(alert, animated: true, completion: nil)

        return false
      }
    }
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnScanQRCodeAction(_ sender: Any) {
        
        
       
//        let errorVC = self.storyboard?.instantiateViewController(withIdentifier: "QRScanErrorVC") as! QRScanErrorVC
//        let navVC = UINavigationController(rootViewController:errorVC )
//        self.present(navVC, animated: true, completion: nil);
     
        
    }
    //---------------------------------------------------
    @IBAction func btnScannedAction(_ sender: Any) {
        
        let guestList = self.storyboard?.instantiateViewController(withIdentifier: "GuestListVC") as! GuestListVC
        guestList.arrJoin = self.arrJoinedAttendees
        guestList.isOpenFromScan = true
        guestList.totalCount = self.totalCount
        self.navigationController?.pushViewController(guestList, animated: true)
    }
    //---------------------------------------------------
    @IBAction func btnNotScannedAction(_ sender: Any) {
        
        let guestList = self.storyboard?.instantiateViewController(withIdentifier: "GuestListVC") as! GuestListVC
        guestList.arrJoin = self.arrRemaining_attendees
        guestList.isOpenFromScan = false
        guestList.totalCount = self.totalCount
        self.navigationController?.pushViewController(guestList, animated: true)
    }
    //---------------------------------------------------
    func callGetSecurityEventsAttendee(_ eventID : String) {
        controller.getSecurityEventsAttendees(eventId: eventID) { (success, response, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    print(response)
                    let dictResponse = response as? [String:Any] ?? [:]
                    let arrTotal_attendees : [[String:Any]] = dictResponse["total_attendees"] as? [[String:Any]] ?? []
                    self.totalCount = arrTotal_attendees.count
                    let arrJoined_attendees : [[String:Any]] = dictResponse["joined_attendees"] as? [[String:Any]] ?? []
                    self.arrJoinedAttendees = arrJoined_attendees
                    let arrRemaining_attendees : [[String:Any]] = dictResponse["remaining_attendees"] as? [[String:Any]] ?? []
                    self.arrRemaining_attendees = arrRemaining_attendees
                    let ratio : Float = Float(arrJoined_attendees.count)/Float(self.totalCount)
                    self.progressView.progress = ratio
                    self.progressView.setProgress(ratio, animated: true)
                    self.lblScanned.text = "\(AppHelper.localizedtext(key: "kScanned"))  \(arrJoined_attendees.count)"
                    self.lblNotScanned.text = "\(AppHelper.localizedtext(key: "kNotScanned")) \(arrRemaining_attendees.count)"
                  //  progressView.ma
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    
    
    //----------------------------------------------------
    func callGetShowEventApi()  {
        controller.getShowEvents { (success, response, message, statusCode) in
            if success{
                DispatchQueue.main.async {
                    print(response)
                    var eventResponse : EventDetail = response as!EventDetail
                    self.eventDetailObj = eventResponse
                    if self.eventDetailObj != nil{
                        self.setDataOfScreen(eventObj: self.eventDetailObj!)
                    }
                    
                }
            }else{
               DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    //--------------------------------------------------------
    
    func setDataOfScreen(eventObj : EventDetail){
        
        self.lblEventName.text = eventObj.name ?? ""
     let strEventDate = eventObj.event_date ?? ""
        let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
               
               let eventDate = dateFormatter.date(from:strEventDate)
               dateFormatter.dateFormat = "dd/MM/yyyy, hh:mm a"
               let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
                self.lblEventDate.text = strEventFinalDate
    }
    
    //------------------------------------------------------
    override func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
