//
//  GuardLoginVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 09/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import CountryPickerView
import PopupDialog

class GuardLoginVC: AppViewController , UITextFieldDelegate{

    //........MARK: Variables......................
    let controller = GuardViewModel()
    var country_code = String()
    let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
    //........MARK: IBOutlets......................
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPhoneCode: BWTextField!
    @IBOutlet weak var txtPhoneNumber: BWTextField!
    @IBOutlet weak var txtPassword: BWTextField!
    
    //--------------------------------------------------------------------
    //MARK: View life cycle
    //--------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true,animated:false)
        self.setTitleView()
 
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        
        self.txtPhoneCode.placeholder = ""
        self.cp.frame = CGRect(x: 10, y: 0, width: self.txtPhoneCode.frame.size.width, height: self.txtPhoneCode.frame.size.height)
        self.txtPhoneCode.addSubview(self.cp);
        self.cp.clipsToBounds = true
        cp.dataSource = self
        cp.delegate = self
        self.country_code = "+966"
        cp.setCountryByCode("+966")
        cp.setCountryByPhoneCode("+966")
        cp.showCountryCodeInView = false
        cp.semanticContentAttribute = .forceLeftToRight
        self.txtPhoneNumber.delegate = self;
        self.cp.centerXAnchor.constraint(equalTo: self.txtPhoneCode.centerXAnchor).isActive = true
        self.cp.centerYAnchor.constraint(equalTo: self.txtPhoneCode.centerYAnchor).isActive = true
    }
    //-------------------------------------------------------
       //MARK: UITextfield delegateMethods
       //-------------------------------------------------------
       func textFieldDidBeginEditing(_ textField: UITextField) {
           textField.bordrColor = UIColor(named: "SecondrayColor")
       }
       //-------------------------------------------------------
       func textFieldDidEndEditing(_ textField: UITextField) {
           textField.bordrColor = UIColor(named: "lightGray")
       }
    //-------------------------------------------------------
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == self.txtPhoneNumber){
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            let length = self.country_code.count - 1
            if(length == 3){
                return count <= 9
            }else{
                return count <= 10
            }
            
        }
        return true
    }
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnLoginAction(_ sender: Any) {
        
//
//        let scanVC = self.storyboard?.instantiateViewController(withIdentifier: "QRScanVC")as! QRScanVC
//        self.navigationController?.pushViewController(scanVC, animated: true)
        var phoneLength : Int = 0
        let length = self.country_code.count-1
        if(length == 3){
            phoneLength = 9
        }else{
            phoneLength = 10
        }
        
        if self.country_code  == "" {
            self.showAlert(message: ValidationAlert.kEmptyPhoneCode, title: kAppName)
        }else if self.txtPhoneNumber.text == "" {
            self.showAlert(message: ValidationAlert.kEmptyPhoneNumber, title: kAppName)
        }else if self.txtPassword.text == "" {
            self.showAlert(message: ValidationAlert.kEmptyPwd, title: kAppName)
        }else if self.txtPhoneNumber.text?.count != phoneLength {
            self.showAlert(title: kAppName, message: ValidationAlert.kInvalidPhone)
        }
        else{
//            self.txtPhoneNumber.text = "8866988196"
//            self.txtPassword.text = "ZsGuptQz"

            self.showActivityIndicatory()
            controller.guardLogin(phoneNumer: self.txtPhoneNumber.text!, countryCode: self.country_code, password: self.txtPassword.text!) { (success, guardObj, message, statusCode) in

                if success{
                    self.hideActivityIndicator()
                    DispatchQueue.main.async {
                     print(guardObj)
                        let scanVC = self.storyboard?.instantiateViewController(withIdentifier: "QRScanVC") as! QRScanVC
                        scanVC.guardObj = guardObj as? Guard
                        self.navigationController?.pushViewController(scanVC, animated: true)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.showAlert(message: message, title: kAppName)
                    }
                }
            }
        }

        
    }
    

}

extension GuardLoginVC : CountryPickerViewDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let title = "Selected Country"
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
      //  self.txtPhoneCode.text = "\(country.phoneCode)"
        self.country_code = "\(country.phoneCode)"
        self.txtPhoneNumber.becomeFirstResponder()
        self.txtPhoneNumber.text = ""
        self.cp.setCountryByName(country.name)
      
    }
}

extension GuardLoginVC : CountryPickerViewDataSource{
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
     //   if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
            var countries = [Country]()
            ["IN", "QA"].forEach { code in
                if let country = countryPickerView.getCountryByCode(code) {
                    countries.append(country)
                }
            }
            return countries
      //  }
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
//        if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
//            return "Preferred title"
//        }
        return nil
    }
    
//    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
//      //  return countryPickerView.tag == cpvMain.tag && showOnlyPreferredCountries.isOn
//    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
        
//    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
////        if countryPickerView.tag == cpvMain.tag {
////            switch searchBarPosition.selectedSegmentIndex {
////            case 0: return .tableViewHeader
////            case 1: return .navigationBar
////            default: return .hidden
////            }
////        }
////        return .tableViewHeader
//    }
    
//    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//        return countryPickerView.tag == cpvMain.tag && showPhoneCodeInList.isOn
//    }
//
//    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//       return countryPickerView.tag == cpvMain.tag && showCountryCodeInList.isOn
//    }
}

