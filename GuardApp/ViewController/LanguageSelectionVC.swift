//
//  LanguageSelectionVC.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 08/04/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit



private var associatedLanguageBundle:Character = "0"

class PrivateBundle: Bundle {
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        let bundle: Bundle? = objc_getAssociatedObject(self, &associatedLanguageBundle) as? Bundle
        return (bundle != nil) ? (bundle!.localizedString(forKey: key, value: value, table: tableName)) : (super.localizedString(forKey: key, value: value, table: tableName))
        
    }
}

extension Bundle {
    class func setLanguage(_ language: String) {
        var onceToken: Int = 0
        
        if (onceToken == 0) {
            /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
            object_setClass(Bundle.main, PrivateBundle.self)
        }
        onceToken = 1
        objc_setAssociatedObject(Bundle.main, &associatedLanguageBundle, (language != nil) ? Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj") ?? "") : nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}






class LanguageSelectionVC: AppViewController {

    var isOpenFromAdmin = Bool()
    var isOpenFromProfile = Bool()
    var isEnglishSelected = Bool()
    var isArabicSelected = Bool()
    var strLanguageSelected = String()
    let imgBlueCircle = UIImage(named: "circle")
    let imgGrayCirle = UIImage(named: "Circle_Gray")
    
 @IBOutlet var btnSave : UIButton!
    @IBOutlet var btnArabic : UIButton!
    @IBOutlet var btnEnglish : UIButton!
    @IBOutlet var btnContinue : UIButton!
    @IBOutlet var lblLanguage : UILabel!
    @IBOutlet var lblArabic : UILabel!
    @IBOutlet var lblEnglish : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.strLanguageSelected =  GlobalUtils.getInstance().getApplicationLanguage()
        
        self.title = AppHelper.localizedtext(key: "kQRCodeInvite")
        if self.isOpenFromProfile{
            self.navigationItem.setHidesBackButton(false, animated: false)
            self.btnContinue.isHidden = true
          self.btnSave.isHidden = false
             self.btnSave.setTitle(AppHelper.localizedtext(key: "kSave"), for: .normal);
            let strLanguage = GlobalUtils.getInstance().getApplicationLanguage()
          switch strLanguage {
            case "english":
                self.isEnglishSelected = true
                setLanguage(self.isEnglishSelected)
                break
            case "arabic":
                self.isEnglishSelected = false
                setLanguage(self.isEnglishSelected)
                break
            default:
                break
            }
            
        }else{
            self.btnContinue.isHidden = false
          self.btnSave.isHidden = true
            self.navigationItem.setHidesBackButton(true, animated: false)
            self.lblArabic.translatesAutoresizingMaskIntoConstraints = false
        }
          UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: Font.barButtonFont], for: .normal)
       self.setTitleView()
        
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = AppHelper.localizedtext(key: "kQRCodeInvite")
        let str_language : String = GlobalUtils.getInstance().getApplicationLanguage()
                if str_language == "english"{
                    
                    self.lblArabic.textAlignment = .left
                    self.lblEnglish.textAlignment = .left
//                    self.lblArabic.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
//                    self.lblEnglish.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
                }else{
        //            self.btnNext.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        //            self.btnPrevious.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
//
                     self.lblEnglish.textAlignment = .left
                    self.lblArabic.textAlignment = .left
                }
        
    }
    
    @IBAction func btnArabicAction(_ sender: Any) {
        
        self.lblArabic.translatesAutoresizingMaskIntoConstraints = false
        if self.isArabicSelected{
            self.isArabicSelected = false
            self.isEnglishSelected = true
            self.navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
            self.btnEnglish.setImage(imgBlueCircle, for: .normal)
            self.btnArabic.setImage(imgGrayCirle, for: .normal)
            self.strLanguageSelected = "english"
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            
        }else{
            
            self.isArabicSelected = true
            self.isEnglishSelected = false
            self.btnArabic.setImage(imgBlueCircle, for: .normal)
            self.btnEnglish.setImage(imgGrayCirle, for: .normal)
            self.strLanguageSelected = "arabic"
            self.navigationController?.navigationBar.semanticContentAttribute = .forceRightToLeft
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        
        
        
        //-------------
        
        
//
//                if self.isEnglishSelected{
//                         self.isEnglishSelected = false
//                         //graybutton
//                         self.btnArabic.setImage(imgBlueCircle, for: .normal)
//                         self.btnEnglish.setImage(imgGrayCirle, for: .normal)
//                         self.strLanguageSelected = "arabic"
//
//                        UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                     }else{
//                         self.isEnglishSelected = true
//                         //blue buton
//                         self.btnEnglish.setImage(imgBlueCircle, for: .normal)
//                         self.btnArabic.setImage(imgGrayCirle, for: .normal)
//                         self.strLanguageSelected = "english"
//
//                         UIView.appearance().semanticContentAttribute = .forceLeftToRight
//
//    }
        self.setDataAccordingToLanguage()
    }
    
    @IBAction func btnEnglishAction(_ sender: Any) {
        
        
        if self.isEnglishSelected{
                  self.isEnglishSelected = false
            self.isArabicSelected = true
                  
                   self.btnArabic.setImage(imgBlueCircle, for: .normal)
                  self.btnEnglish.setImage(imgGrayCirle, for: .normal)
                  self.strLanguageSelected = "arabic"
                  UIView.appearance().semanticContentAttribute = .forceRightToLeft
                  self.navigationController?.navigationBar.semanticContentAttribute = .forceRightToLeft
                 
              }else{
                self.isEnglishSelected = true
                self.isArabicSelected = false
                self.btnEnglish.setImage(imgBlueCircle, for: .normal)
                self.btnArabic.setImage(imgGrayCirle, for: .normal)
                self.strLanguageSelected = "english"
             UIView.appearance().semanticContentAttribute = .forceLeftToRight
            self.navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
           
                  
              }
        self.setDataAccordingToLanguage()
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
        if self.strLanguageSelected == "" {
            self.showAlert(message: ValidationAlert.kEmptyLanguageSelect, title: kAppName)
        }else{
            GlobalUtils.getInstance().setApplicationLanguage(language: self.strLanguageSelected);
            if(self.strLanguageSelected == "arabic"){
                Bundle.setLanguage("ar")
                

                UIView.appearance().semanticContentAttribute = .forceRightToLeft
               // LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
            }else{
                
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                Bundle.setLanguage("en")
                //LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            }
            if self.isOpenFromProfile{
                self.navigationController?.popViewController(animated: true)
              //  self.navigationController?.popToRootViewController(animated: true)
                //self.tabBarController?.selectedIndex = 1
               // self.tabBarController?.tabBar.semanticContentAttribute = .forceLeftToRight

            }else{
                
                let chooseLoginRole = self.storyboard?.instantiateViewController(withIdentifier: "LoginRoleVC") as! LoginRoleVC
                self.navigationController?.pushViewController(chooseLoginRole, animated: true)
                
                
               // if self.isOpenFromAdmin{
          //          let onBoarding = self.storyboard?.instantiateViewController(withIdentifier: "AdminLoginVC")as! AdminLoginVC
         //           self.navigationController?.pushViewController(onBoarding, animated: true)
        //        }else{
      //              let onBoarding = self.storyboard?.instantiateViewController(withIdentifier: "GuardLoginVC")as! GuardLoginVC
          //          self.navigationController?.pushViewController(onBoarding, animated: true)
         //       }
                
               
            }
            
        }
        
    }
    
    //--------------------------------------------------------
    //MARK: Custom Method
    //--------------------------------------------------------
    func setDataAccordingToLanguage()  {
         
         self.title = AppHelper.localizedtext(key: "kQRCodeInvite")
        if strLanguageSelected == "english"{
            self.btnSave.setTitle("Save", for: .normal);
            self.btnContinue.setTitle("Continue", for: .normal)
            self.lblLanguage.text = "Please Select The Language"
            self.lblEnglish.text = "English"
            self.lblArabic.text = "Arabic"
            self.title = "QR Card Invite"
        }else{
           self.btnSave.setTitle("حفظ", for: .normal);
            self.btnContinue.setTitle("استمر", for: .normal)
            self.lblLanguage.text = "يرجى اختيار اللغة المفضلة"
            self.lblEnglish.text =  "الإنجليزية"
            self.lblArabic.text = "عربى"
            self.title =  "دعوة بطاقة QR"
        }
    }
    //----------------------------
    func setLanguage(_ englishSelected : Bool)  {
        
        
        if englishSelected{
            self.btnEnglish.setImage(imgBlueCircle, for: .normal)
            self.btnArabic.setImage(imgGrayCirle, for: .normal)
            self.strLanguageSelected = "english"
            
        }else{
            self.btnArabic.setImage(imgBlueCircle, for: .normal)
            self.btnEnglish.setImage(imgGrayCirle, for: .normal)
             self.strLanguageSelected = "arabic"
            
        }
        self.setDataAccordingToLanguage()
    }
}
