//
//  LoginRoleVC.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 10/04/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit

class LoginRoleVC: AppViewController {

    
    //.......MARK: Variables..........................
    
    
    
    
    //.......MARK: IBOutlet..........................
    
    @IBOutlet weak var btnLoginGuard : UIButton!
    @IBOutlet weak var btnLoginAdmin : UIButton!
    
    
    
    
    //--------------------------------------------------
    //MARK: View Life Cycle
    //--------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitleView()
    }
    
    //--------------------------------------------------
    //MARK: IBAction Of UIButton
    //--------------------------------------------------
    @IBAction func btnLoginGuard(_ sender:Any){
       
        let guardLogin = self.storyboard?.instantiateViewController(withIdentifier: "GuardLoginVC") as! GuardLoginVC
        self.navigationController?.pushViewController(guardLogin, animated: true)
        
    }
    //--------------------------------------------------
    @IBAction func btnLoginAdmin(_ sender:Any){
        
        let adminLogin = self.storyboard?.instantiateViewController(withIdentifier: "AdminLoginVC") as! AdminLoginVC
        self.navigationController?.pushViewController(adminLogin, animated: true)
               
    }
    

}
