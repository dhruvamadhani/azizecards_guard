//
//  OngoingOrCompletedTableCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 18/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class OngoingOrCompletedTableCell: UITableViewCell {

    @IBOutlet weak var imgRightArrow : UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var viewGreen : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
