//
//  CommentTableCell.swift
//  GuardApp
//
//  Created by Dhruva Madhani on 17/04/20.
//  Copyright © 2020 vandan. All rights reserved.
//

import UIKit



class CommentTableCell: UITableViewCell {

    @IBOutlet weak var lblComment : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
